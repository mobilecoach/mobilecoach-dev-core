package ch.ethz.mc.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ch.ethz.mc.conf.Constants;
import ch.ethz.mc.conf.ImplementationConstants;
import ch.ethz.mc.model.Queries;
import ch.ethz.mc.model.memory.RuleEvaluationResult;
import ch.ethz.mc.model.persistent.GlookoFoodsAPINotification;
import ch.ethz.mc.model.persistent.ScreeningSurveySlideRule;
import ch.ethz.mc.model.persistent.concepts.AbstractMonitoringRule;
import ch.ethz.mc.model.persistent.concepts.AbstractRule;
import ch.ethz.mc.services.internal.DatabaseManagerService;
import ch.ethz.mc.services.internal.VariablesManagerService;
import ch.ethz.mc.services.threads.GlookoDataWorker;
import ch.ethz.mc.tools.RuleEvaluator;
import lombok.val;
import lombok.extern.log4j.Log4j2;

/**
 * Cares for getting the data from the Glooko server. 
 *
 * @author Prabhakaran Santhanam
 */
@Log4j2
public class GlookoDataManagerService {
	
	private static GlookoDataManagerService instance				    = null;
	
	private final DatabaseManagerService    databaseManagerService;
	private final VariablesManagerService   variablesManagerService;
	private final boolean                   isGlookoIntegrationActive;
	private String 							glookoAccountUsername;
	private String							glookoAccountPassword;
	private String							glookoAPIKey;
	private String							glookoAPIURL;
	private String							glookoLoginURL; 
	private String							glookoJWTAccessToken;
	private long							glookoLastAccessTokenTimeStamp;
	private long 							recursiveCounter;
	private int 							splitByDays;
	
	private final GlookoDataWorker			glookoDataWorker;
	private final Queue <GlookoRequest> 	requestQueue;
	
	
	private GlookoDataManagerService(
			final DatabaseManagerService databaseManagerService,
			final VariablesManagerService variablesManagerService)
				throws Exception {
		log.info("Starting Glooko service...");
		
		this.databaseManagerService = databaseManagerService;
		this.variablesManagerService = variablesManagerService;
		this.isGlookoIntegrationActive = Constants.isGlookoIntegrationActive();
		this.glookoAccountUsername = Constants.getGlookoAccountUsername();
		this.glookoAccountPassword = Constants.getGlookoAccountPassword();
		this.glookoAPIKey = Constants.getGlookoAPIKey();
		this.glookoAPIURL = Constants.getGlookoAPIURL();
		this.glookoLoginURL = Constants.getGlookoAPIURL() + ImplementationConstants.GLOOKO_API_LOGIN_URL;
		this.glookoJWTAccessToken = "";
		this.glookoLastAccessTokenTimeStamp = 0;
		this.splitByDays = Constants.getGlookoSplitByDays();
		
		glookoDataWorker = new GlookoDataWorker(this);
		requestQueue = new LinkedList<GlookoRequest>();
		
		log.info("Glooko started.");
	}
	
	
	public static GlookoDataManagerService start(
			final DatabaseManagerService databaseManagerService,
			final VariablesManagerService variablesManagerService)
			throws Exception {
		if (instance == null) {
			instance = new GlookoDataManagerService(
					databaseManagerService,
					variablesManagerService);
		}
	
		RuleEvaluator.setGlookoDataManagerService(instance);
		return instance;
	}
	
	public void startWorking() {
		new Thread("Glooko Worker Starter") {
			@Override
			public void run() {
				log.info("Starting Glooko worker...");

				// Start Glooko manager service
				try {
					glookoDataWorker.start();
				} catch (final Exception e) {
					log.error("Error at starting glooko worker: {}",
							e.getMessage());
				}

				log.info("Started.");
			}
		}.start();
	}
	
	public void stop() throws Exception {
		log.info(
				"Stopping glooko worker and and service (takes several seconds)...");

		synchronized (glookoDataWorker) {
			glookoDataWorker.setShouldStop(true);
			Thread.sleep(2000);
			try {
				glookoDataWorker.interrupt();
				glookoDataWorker.join();
			} catch (final Exception e) {
				// Do nothing
			}
		}
		log.info("Stopped.");
	}
			
	
	public void generateRequest(
			final ObjectId participantId, 
			final AbstractRule rule, 
			final RuleEvaluationResult ruleEvaluationResult) {
		log.debug("Generating a new request...");
		GlookoRequest newRequest = new GlookoRequest(participantId, rule, ruleEvaluationResult);
		// TODO: find out why you need a synchronized over requestQueue 
		synchronized (requestQueue) {
			requestQueue.add(newRequest);	
		}
	}
	
	/**
	 *
	 * @author Prabhakaran Santhanam
	 */
	private class GlookoRequest {
		private final ObjectId participantId;
		private final AbstractRule rule; 
		private final RuleEvaluationResult ruleEvaluationResult;
		
		GlookoRequest (
			final ObjectId participantId, 
			final AbstractRule rule, 
			final RuleEvaluationResult ruleEvaluationResult) {
			
			this.participantId = participantId;
			this.rule = rule;
			this.ruleEvaluationResult = ruleEvaluationResult;
		}
	}
	
	/**
	 *
	 * @author Prabhakaran Santhanam
	 */
	private class SplitDate {
		private final String startDate;
		private final String endDate;
		
		SplitDate (
			final String startDate, 
			final String endDate) {
			
			this.startDate = startDate;
			this.endDate = endDate;
		}
	}


	public void handleGlookoRequests() {
		while(!requestQueue.isEmpty()) {
			GlookoRequest currentRequest = requestQueue.poll();
			
			// See MC.java: if the integration is not active, this service won't be called.
			if (!isGlookoIntegrationActive) {
				log.warn("The glooko service is disabled in the configuration file. Hence the requests are ignored.");
				continue;
			}
			
			if(currentRequest!=null) {		
				if(checkAndSetGlookoJWTAccessToken()) {
					log.info("Processing request...");
					try {
						
						String [] requestInfoTokens;
						String getFromTimeStamp, getToTimeStamp, glookoCode, URL, response, writableData, timezoneDiffInMinues;
						
						switch (currentRequest.rule.getRuleEquationSign()) {
							case BGM_ASYNC_GET_GLUCOSE_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
										currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim() + "T00:00:00.000Z";
									getToTimeStamp = requestInfoTokens[1].trim() + "T00:00:00.000Z";
									glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
									URL = glookoAPIURL + ImplementationConstants.GLOOKO_BGM_GET_DATA_URL + "?" 
												 + "patient=" + glookoCode + "&"
												 + "startDate=" + getFromTimeStamp + "&"
												 + "endDate=" + getToTimeStamp;
									
									log.info("The request URI : " + URL);
									response = queryGlookoAPI(URL);
									
									if (response != null) {
										log.debug("The response is : " + response);
										writableData = processBGMData(response);
										
										if(writableData != null) {
											log.debug("The data to write over the variable is: " + writableData);
											// Writing to the variable
											writeDataToVariable(writableData, currentRequest.participantId, currentRequest.rule);
										}
										else { 
											log.error("The Glooko data could not be converted properly to put in the variables.");
											writeDataToVariable(
													"Error: " + System.currentTimeMillis(), 
													currentRequest.participantId, 
													currentRequest.rule);
										}
										
									}
									else {
										log.error("Response is null, error while processing the request.");
										writeDataToVariable(
												"Error: " + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}	
								}
								catch(Exception e) {
									log.error("Error occurred whle processing rule (BGM async - get data): " +e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
							
							case BGM_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
											currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim();
									getToTimeStamp = requestInfoTokens[1].trim();
									
									String path = "/mc_data/glooko_data/" + currentRequest.participantId + "/BGM/";
									File directory = new File(path); 
									if (!directory.exists()) directory.mkdirs();
									
									FileWriter fileWriter = new FileWriter(path + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									
									List <SplitDate> splitDates = splitDates(getFromTimeStamp, getToTimeStamp); 
									
									if (splitDates != null) {
										for (SplitDate splitDate: splitDates) {
											glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_BGM_GET_DATA_URL + "?" 
														 + "patient=" + glookoCode + "&"
														 + "startDate=" + splitDate.startDate + "&"
														 + "endDate=" + splitDate.endDate;
											
											log.info("The request URI : " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriter.append(writableData);
													fileWriter.append("\n");
												}
											}
										}
										fileWriter.close();
										writeDataToVariable(
												"BGM data downloaded for "+ currentRequest.participantId +" : @" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									else {
										fileWriter.close();
										log.error("The input dates are not valid.");
										writeDataToVariable(
												"Error: " + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									
								} catch(Exception e) {
									log.error("Error occurred whle processing rule (BGM async - get data and write into file): " 
											+ e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;

							case GLOOKO_APP_ASYNC_GET_CARBS_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
										currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim() + "T00:00:00.000Z";
									getToTimeStamp = requestInfoTokens[1].trim() + "T00:00:00.000Z";
									timezoneDiffInMinues = requestInfoTokens[2].trim();
									
									glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
									
									// Add the offset +/-7 days for letting the sync to happen later
									long getFromTimeStampInEpoch = Instant.parse(getFromTimeStamp).toEpochMilli() - 7*24*60*60*1000;
									long getToTimeStampInEpoch = Instant.parse(getToTimeStamp).toEpochMilli() + 7*24*60*60*1000;
									
									// Getting the Sync time stamps for this Glooko code
									List <String> syncTimeStamps = new ArrayList <String> ();
									val glookoFoodsAPINotifications = databaseManagerService.findModelObjects(
											GlookoFoodsAPINotification.class, 
											Queries.GLOOKO_FOODS_API_NOTIFICATION_BY_GLOOKO_CODE,
											glookoCode);
									
									for (val glookoFoodsAPINotification : glookoFoodsAPINotifications) {
										String syncTimeStamp = glookoFoodsAPINotification.getSyncTimestamp();
										
										long syncTimeStampInEpoch = Instant.parse(syncTimeStamp).toEpochMilli();
										
										if (syncTimeStampInEpoch >= getFromTimeStampInEpoch 
												&& syncTimeStampInEpoch < getToTimeStampInEpoch) {
											syncTimeStamps.add(syncTimeStamp);
										}
									}
									
									// Get the data
									writableData = "";
									for (String syncTimeStamp: syncTimeStamps) {
										URL = glookoAPIURL + ImplementationConstants.GLOOKO_APP_GET_DATA_URL + "?" 
												 + "patient=" + glookoCode + "&"
												 + "syncTimestamp=" + syncTimeStamp; 
										log.info("The request URI : " + URL);
										response = queryGlookoAPI(URL);
										if (response != null) {
											if (writableData == "") 
												writableData = processFoodsAPIDataWithTZAdjustedUpdatedAt(response, timezoneDiffInMinues, 
														getFromTimeStamp, getToTimeStamp);
											else 
												writableData = writableData + ", " 
														+ processFoodsAPIDataWithTZAdjustedUpdatedAt(response, timezoneDiffInMinues, 
																getFromTimeStamp, getToTimeStamp);
										} 
										else {
											log.error("Response is null, error while processing the request.");
										}
									}
									
									writeDataToVariable(writableData, currentRequest.participantId, currentRequest.rule);
								}
								catch(Exception e) {
									log.error("Error occurred whle processing rule (Glooko app async - get data): " +e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
							
							case CGM_ASYNC_GET_GLUCOSE_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
										currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim() + "T00:00:00.000Z";
									getToTimeStamp = requestInfoTokens[1].trim() + "T00:00:00.000Z";
									glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
									URL = glookoAPIURL + ImplementationConstants.GLOOKO_CGM_GET_DATA_URL + "?" 
												 + "patient=" + glookoCode + "&"
												 + "startDate=" + getFromTimeStamp + "&"
												 + "endDate=" + getToTimeStamp;
									
									log.info("The request URI : " + URL);
									response = queryGlookoAPI(URL);
									
									if (response != null) {
										log.debug("The response is : " + response);
										writableData = processCGMData(response);
										
										if(writableData != null) {
											log.debug("The data to write over the variable is: " + writableData);
											// Writing to the variable
											writeDataToVariable(writableData, currentRequest.participantId, currentRequest.rule);
										}
										else { 
											log.error("The Glooko data could not be converted properly to put in the variables.");
											writeDataToVariable(
													"Error:" + System.currentTimeMillis(), 
													currentRequest.participantId, 
													currentRequest.rule);
										}
										
									}
									else {
										log.error("Response is null, error while processing the request.");
										writeDataToVariable(
												"Error:" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}	
								} 
								catch (Exception e) {
									log.error("Error occurred whle processing rule (CGM async - get data): " +e.getMessage());
									writeDataToVariable(
											"Error:" + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
								
							case CGM_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
											currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim();
									getToTimeStamp = requestInfoTokens[1].trim();
									
									String path = "/mc_data/glooko_data/" + currentRequest.participantId + "/CGM/";
									File directory = new File(path); 
									if (!directory.exists()) directory.mkdirs();
									
									FileWriter fileWriter = new FileWriter(path + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									
									List <SplitDate> splitDates = splitDates(getFromTimeStamp, getToTimeStamp); 
									
									if (splitDates != null) {
										for (SplitDate splitDate: splitDates) {
											glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_CGM_GET_DATA_URL + "?" 
														 + "patient=" + glookoCode + "&"
														 + "startDate=" + splitDate.startDate + "&"
														 + "endDate=" + splitDate.endDate;
											
											log.info("The request URI : " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriter.append(writableData);
													fileWriter.append("\n");
												}
											}
										}
										fileWriter.close();
										writeDataToVariable(
												"CGM data downloaded for "+ currentRequest.participantId +" : @" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									else {
										fileWriter.close();
										log.error("The input dates are not valid.");
										writeDataToVariable(
												"Error: " + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									
								} catch(Exception e) {
									log.error("Error occurred whle processing rule (CGM async - get data and write into file): " 
											+ e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
							
							case CGM_ASYNC_GET_WEARTIME_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
										currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim() + "T00:00:00.000Z";
									getToTimeStamp = requestInfoTokens[1].trim() + "T00:00:00.000Z";
									glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
									URL = glookoAPIURL + ImplementationConstants.GLOOKO_CGM_GET_WEARTIME_URL + "?" 
												 + "patient=" + glookoCode + "&"
												 + "startDate=" + getFromTimeStamp + "&"
												 + "endDate=" + getToTimeStamp;
									
									log.info("The request URI : " + URL);
									response = queryGlookoAPI(URL);
									
									if (response != null) {
										log.debug("The response is : " + response);
										writableData = getWearTimeFromCGMStatistics(response).trim();
										
										if(writableData != null) {
											log.debug("The data to write over the variable is: " + writableData);
											// Writing to the variable
											writeDataToVariable(writableData, currentRequest.participantId, currentRequest.rule);
										}
										else { 
											log.error("The Glooko data could not be converted properly to put in the variables.");
											writeDataToVariable(
													"Error:" + System.currentTimeMillis(), 
													currentRequest.participantId, 
													currentRequest.rule);
										}
										
									}
									else {
										log.error("Response is null, error while processing the request.");
										writeDataToVariable(
												"Error:" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}	
								} 
								catch (Exception e) {
									log.error("Error occurred whle processing rule (CGM async - get weartime): " +e.getMessage());
									writeDataToVariable(
											"Error:" + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
								
							case IP_ASYNC_GET_GLUCOSE_NON_MANUAL_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
										currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim() + "T00:00:00.000Z";
									getToTimeStamp = requestInfoTokens[1].trim() + "T00:00:00.000Z";
									glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
									URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_GLUCOSE_URL + "?" 
												 + "patient=" + glookoCode + "&"
												 + "startDate=" + getFromTimeStamp + "&"
												 + "endDate=" + getToTimeStamp;
									
									log.info("The request URI : " + URL);
									response = queryGlookoAPI(URL);
									
									if (response != null) {
										log.debug("The response is : " + response);
										writableData = processIPGlucoseNonManualData(response);
										
										if(writableData != null) {
											log.debug("The data to write over the variable is: " + writableData);
											// Writing to the variable
											writeDataToVariable(writableData, currentRequest.participantId, currentRequest.rule);
										}
										else { 
											log.error("The Glooko data could not be converted properly to put in the variables.");
											writeDataToVariable(
													"Error: " + System.currentTimeMillis(), 
													currentRequest.participantId, 
													currentRequest.rule);
										}
										
									}
									else {
										log.error("Response is null, error while processing the request.");
										writeDataToVariable(
												"Error: " + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}	
								}
								catch(Exception e) {
									log.error("Error occurred whle processing rule (BGM async - get data): " +e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
								
							case IP_ASYNC_GET_CARBS_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE:
								try {
									String writableDataNormal, writableDataExtended, responseNormal, responseExtended;
									
									requestInfoTokens = 
										currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim() + "T00:00:00.000Z";
									getToTimeStamp = requestInfoTokens[1].trim() + "T00:00:00.000Z";
									
									// Normal Boluses data
									glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
									
									URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_NORMAL_BOLUSES_URL + "?" 
												 + "patient=" + glookoCode + "&"
												 + "startDate=" + getFromTimeStamp + "&"
												 + "endDate=" + getToTimeStamp;
									
									log.info("The request URI (Normal) : " + URL);
									responseNormal = queryGlookoAPI(URL);
									
									URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_EXTENDED_BOLUSES_URL + "?" 
											 + "patient=" + glookoCode + "&"
											 + "startDate=" + getFromTimeStamp + "&"
											 + "endDate=" + getToTimeStamp;
								
									log.info("The request URI (Extended) : " + URL);
									responseExtended = queryGlookoAPI(URL);	
										
									
									if (responseNormal != null && responseExtended != null) {
										log.debug("The response (Normal) is : " + responseNormal);
										log.debug("The response (Extended) is : " + responseExtended);
										writableDataNormal = processIPNormalBolusesData(responseNormal).trim();
										writableDataExtended = processIPExtendedBolusesData(responseExtended).trim();
										
										if(writableDataNormal != null && writableDataExtended != null) {
											if (writableDataNormal.isEmpty() && !writableDataExtended.isEmpty()) 
												writableData = writableDataExtended;
											else if (!writableDataNormal.isEmpty() && writableDataExtended.isEmpty()) 
												writableData = writableDataNormal;
											else if (!writableDataNormal.isEmpty() && !writableDataExtended.isEmpty())
												writableData = writableDataNormal + ", " + writableDataExtended;
											else 
												writableData = "";
											
											log.debug("The data to write over the variable is: " + writableData);
											// Writing to the variable
											writeDataToVariable(writableData, currentRequest.participantId, currentRequest.rule);
										}
										else { 
											log.error("The Glooko data could not be converted properly to put in the variables.");
											writeDataToVariable(
													"Error:" + System.currentTimeMillis(), 
													currentRequest.participantId, 
													currentRequest.rule);
										}
										
									}
									else {
										log.error("Response is null, error while processing the request.");
										writeDataToVariable(
												"Error:" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}	
								} 
								catch (Exception e) {
									log.error("Error occurred whle processing rule (IP async - get data): " +e.getMessage());
									writeDataToVariable(
											"Error:" + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
								
							case IP_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
											currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim();
									getToTimeStamp = requestInfoTokens[1].trim();
									
									String pathNormal = "/mc_data/glooko_data/" + currentRequest.participantId + "/IP/NormalBoluses/";
									String pathExtended = "/mc_data/glooko_data/" + currentRequest.participantId + "/IP/ExtendedBoluses/";
									String pathGlucose = "/mc_data/glooko_data/" + currentRequest.participantId + "/IP/Glucose/";
									String pathSettings = "/mc_data/glooko_data/" + currentRequest.participantId + "/IP/Settings/";
									
									File directoryNormal = new File(pathNormal); 
									File directoryExtended = new File(pathExtended);
									File directoryGlucose = new File(pathGlucose);
									File directorySettings = new File(pathSettings);
									
									if (!directoryNormal.exists()) directoryNormal.mkdirs();
									if (!directoryExtended.exists()) directoryExtended.mkdirs();
									if (!directoryGlucose.exists()) directoryGlucose.mkdirs();
									if (!directorySettings.exists()) directorySettings.mkdirs();
									
									
									FileWriter fileWriterNormal = new FileWriter(pathNormal + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									FileWriter fileWriterExtended = new FileWriter(pathExtended + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									FileWriter fileWriterGlucose = new FileWriter(pathGlucose + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									FileWriter fileWriterSettings = new FileWriter(pathSettings + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									
									List <SplitDate> splitDates = splitDates(getFromTimeStamp, getToTimeStamp); 
									
									if (splitDates != null) {
										for (SplitDate splitDate: splitDates) {
											glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
											
											// Normal Boluses data
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_NORMAL_BOLUSES_URL + "?" 
														 + "patient=" + glookoCode + "&"
														 + "startDate=" + splitDate.startDate + "&"
														 + "endDate=" + splitDate.endDate;
											
											log.info("The request URI (Normal): " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriterNormal.append(writableData);
													fileWriterNormal.append("\n");
												}
											}
											
											// Extended Boluses data
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_EXTENDED_BOLUSES_URL + "?" 
														 + "patient=" + glookoCode + "&"
														 + "startDate=" + splitDate.startDate + "&"
														 + "endDate=" + splitDate.endDate;
											
											log.info("The request URI (Extended): " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriterExtended.append(writableData);
													fileWriterExtended.append("\n");
												}
											}	
											
											// Glucose data
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_GLUCOSE_URL + "?" 
														 + "patient=" + glookoCode + "&"
														 + "startDate=" + splitDate.startDate + "&"
														 + "endDate=" + splitDate.endDate;
											
											log.info("The request URI (Glucose from Insulin Pump): " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriterGlucose.append(writableData);
													fileWriterGlucose.append("\n");
												}
											}
											
											// Settings data
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_IP_GET_DATA_SETTINGS_URL + "?" 
													 + "patient=" + glookoCode + "&"
													 + "startDate=" + splitDate.startDate + "&"
													 + "endDate=" + splitDate.endDate;
											log.info("The request URI (Insulin pump - Settings): " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriterSettings.append(writableData);
													fileWriterSettings.append("\n");
												}
											}
											
										}
										fileWriterExtended.close();
										fileWriterNormal.close();
										fileWriterGlucose.close();
										fileWriterSettings.close();
										
										writeDataToVariable(
												"IP data downloaded for "+ currentRequest.participantId +" : @" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									else {
										fileWriterExtended.close();
										fileWriterNormal.close();
										log.error("The input dates are not valid.");
										writeDataToVariable(
												"Error: " + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									
								} catch(Exception e) {
									log.error("Error occurred whle processing rule (IP async - get data and write into file): " 
											+ e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;
							
							case GLOOKO_APP_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE:
								try {
									requestInfoTokens = 
											currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
									// TODO check if the rule evaluation result has correct format...
									
									getFromTimeStamp = requestInfoTokens[0].trim();
									getToTimeStamp = requestInfoTokens[1].trim();
									
									String path = "/mc_data/glooko_data/" + currentRequest.participantId + "/APP/";
									File directory = new File(path); 
									if (!directory.exists()) directory.mkdirs();
									
									FileWriter fileWriter = new FileWriter(path + currentRequest.participantId + "_" + getFromTimeStamp 
											+ "_" + getToTimeStamp, false);
									
									List <SplitDate> splitDates = splitDates(getFromTimeStamp, getToTimeStamp); 
									
									if (splitDates != null) {
										for (SplitDate splitDate: splitDates) {
											glookoCode = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
											URL = glookoAPIURL + ImplementationConstants.GLOOKO_APP_GET_DATA_URL + "?" 
														 + "patient=" + glookoCode + "&"
														 + "startDate=" + splitDate.startDate + "&"
														 + "endDate=" + splitDate.endDate;
											
											log.info("The request URI : " + URL);
											response = queryGlookoAPI(URL);
											
											if (response != null) {
												log.debug("The response is : " + response);
												writableData = response.trim();
												if (!writableData.isEmpty()) {
													log.debug("Writing the response into file.");
													fileWriter.append(writableData);
													fileWriter.append("\n");
												}
											}
										}
										fileWriter.close();
										writeDataToVariable(
												"Glooko app data (carbs) downloaded for "+ currentRequest.participantId +" : @" + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									else {
										fileWriter.close();
										log.error("The input dates are not valid.");
										writeDataToVariable(
												"Error: " + System.currentTimeMillis(), 
												currentRequest.participantId, 
												currentRequest.rule);
									}
									
								} catch(Exception e) {
									log.error("Error occurred whle processing rule (Glooko app async - get data and write into file): " 
											+ e.getMessage());
									writeDataToVariable(
											"Error: " + System.currentTimeMillis(), 
											currentRequest.participantId, 
											currentRequest.rule);
								}
								break;	
								
							default:
								log.warn("Unknown rule.");
								writeDataToVariable(
										"Error:" + System.currentTimeMillis(), 
										currentRequest.participantId, 
										currentRequest.rule);
								break;
						}
						
						
					} catch (Exception e) {
						log.error("Error while processing the request");
						writeDataToVariable(
								"Error:" + System.currentTimeMillis(), 
								currentRequest.participantId, 
								currentRequest.rule);
					}			
				}
				else {
					log.error("Error while getting the JWT access token. Hence the Glooko rule is not executed.");
					// Setting the timestamp to 0
					glookoLastAccessTokenTimeStamp = 0;
					writeDataToVariable(
							"Error:" + System.currentTimeMillis(), 
							currentRequest.participantId, 
							currentRequest.rule);
				}
				
			}
		}
		
	}


	private List<SplitDate> splitDates(String getFromTimeStamp, String getToTimeStamp) {
		
		LocalDate dateFrom = LocalDate.parse(getFromTimeStamp);
		LocalDate dateTo = LocalDate.parse(getToTimeStamp);
		
		List<SplitDate> splitDates = new ArrayList<SplitDate>();
		
		if (dateFrom.isAfter(dateTo)) {
			log.error("Error: The from-date is later than the to-date.");
			return null;
		}
		
		if (dateFrom.isEqual(dateTo)) {
			log.error("Error: The from-date is equal the to-date.");
			return null;
		}
		
		LocalDate currentFromDate = dateFrom;
		while (currentFromDate.isBefore(dateTo)) {
			LocalDate nextDate = currentFromDate.plusDays(splitByDays);
			if (nextDate.isAfter(dateTo)) {
				nextDate = dateTo;
				splitDates.add(new SplitDate(currentFromDate.format(DateTimeFormatter.ISO_LOCAL_DATE) + "T00:00:00.000Z", 
						nextDate.format(DateTimeFormatter.ISO_LOCAL_DATE) + "T00:00:00.000Z"));
				break;
			}
			
			splitDates.add(new SplitDate(currentFromDate.format(DateTimeFormatter.ISO_LOCAL_DATE) + "T00:00:00.000Z", 
					nextDate.format(DateTimeFormatter.ISO_LOCAL_DATE) + "T00:00:00.000Z"));
		
			currentFromDate = nextDate;
		}
		
		if (splitDates.isEmpty()) {
			log.error("Error: The dates could not be split properly (Empty).");
			return null;
		}
		
		return splitDates;
	}

	private long timestampWithSecondsAndMillisecondsRemoved(long timestamp) {
		return (timestamp/(1000*60))*1000*60;
	}

	private boolean checkAndSetGlookoJWTAccessToken() {
		// Note: Using system timestamp instead of internal time stamp
		val currentTimeStamp = System.currentTimeMillis();
		if (currentTimeStamp - glookoLastAccessTokenTimeStamp 
				> ImplementationConstants.GLOOKO_JWT_ACCESS_TOKEN_RENEW_EVERY_MILLIS) {
			log.info("Time to get a new JWT access token");
			
			try {
				URL url = new URL(glookoLoginURL);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/json");

				/* Payload support */
				con.setDoOutput(true);
				DataOutputStream out = new DataOutputStream(con.getOutputStream());
				out.writeBytes("{\n");
				out.writeBytes("  \"username\": \"" + glookoAccountUsername + "\",\n");
				out.writeBytes("  \"password\": \"" + glookoAccountPassword + "\"\n");
				out.writeBytes("}");
				out.flush();
				out.close();

				int status = con.getResponseCode();
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while((inputLine = in.readLine()) != null) {
					content.append(inputLine);
				}
				in.close();
				con.disconnect();
				
				if(status == 200) {
					log.debug("Response status: " + status);
					log.debug(content.toString());
					JsonObject jsonObject = (JsonObject) new JsonParser().parse(content.toString());
					val tokenJsonObject = jsonObject.get("token");
					
					if(jsonObject.get("token") == null) {
						log.error("Returned token is null. Contact admin.");
						log.error("The error message from Glooko login API is: " + content);
						return false;
					}
					else {
						glookoJWTAccessToken = tokenJsonObject.getAsString();
						glookoLastAccessTokenTimeStamp = System.currentTimeMillis();
						log.debug("Latest JWT access token: " + glookoJWTAccessToken);
						log.debug("Latest JWT access token timestamp: " + glookoLastAccessTokenTimeStamp);
						return true;
					}
				}
				else {
					log.error("Response status of the JWT access token request is:" + status);
					return false;
				}
				
			} catch (Exception e) {
				log.error(e.getMessage());
				return false;
			}
			
		}
		
		return true;
	}
	
	private String queryGlookoAPI(String URL) {
		try {
			URL url = new URL(URL);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("x-api-key", glookoAPIKey);
			con.setRequestProperty("Authorization", "Bearer " + glookoJWTAccessToken);

			int status = con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();
			con.disconnect();
			
			if(status != 200) {
				log.error("The request failed and the resposne status is: " + status);
				log.error("The error message from Glooko API is: " + content);
				return null;
			}
			return content.toString();
			
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}

	private String processBGMData(String response) {
		// Initializing with empty string in case there is no data in the response
		StringBuffer writableData = new StringBuffer("");
		try {
			JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
			JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("readings");
			int count = 0;
			for (JsonElement element : jsonArray) {
				String syncTimestamp, timestamp, value, units, timeOffset;
				
				JsonObject elementJsonObject = element.getAsJsonObject();
				
				JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
				if (!jsonElement.isJsonNull()) syncTimestamp = jsonElement.getAsString();
		        else syncTimestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("timestamp");
				if (!jsonElement.isJsonNull()) timestamp = jsonElement.getAsString();
		        else timestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("value");
				if (!jsonElement.isJsonNull()) value = jsonElement.getAsString();
		        else continue; // if the value is null, need not store that value
				
				// Support for High and low values
	        	if (value.toLowerCase().equals("HI".toLowerCase())) value = "1000";
	        	else if (value.toLowerCase().equals("LO".toLowerCase())) value = "0";
				
				jsonElement = elementJsonObject.get("units");
				if (!jsonElement.isJsonNull()) units = jsonElement.getAsString();
		        else units = "null";
				
				jsonElement = elementJsonObject.get("timeOffset");
				if (!jsonElement.isJsonNull()) timeOffset = jsonElement.getAsString();
		        else timeOffset = "2000-01-01T00:00:00.000Z";
				
				if (count == 0) writableData.append("(" 
					+ syncTimestamp + ", " 
					+ timestamp + ", " 
					+ value + ", " 
					+ units + ", "
					+ timeOffset + ")");
				else writableData.append(", (" 
					+ syncTimestamp + ", " 
					+ timestamp + ", " 
					+ value + ", " 
					+ units + ", "
					+ timeOffset + ")");
				count++;
			}
			return writableData.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	private String processCGMData(String response) {
	    // Initializing with empty string in case there is no data in the response
	    StringBuffer writableData = new StringBuffer("");
	    try {
	      JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
	      JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("cgmReadings");
	      int count = 0;
	      for (JsonElement element : jsonArray) {
	    	String syncTimestamp, systemTime, glucoseValue, units, displayTime;
	    	  
	        JsonObject elementJsonObject = element.getAsJsonObject();
	        
	        JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
	        if (!jsonElement.isJsonNull()) syncTimestamp = jsonElement.getAsString();
	        else syncTimestamp = "2000-01-01T00:00:00.000Z";
	        
	        jsonElement = elementJsonObject.get("systemTime");
	        if (!jsonElement.isJsonNull()) systemTime = jsonElement.getAsString();
	        else systemTime = "2000-01-01T00:00:00.000Z";
	        
	        jsonElement = elementJsonObject.get("glucoseValue");
	        if (!jsonElement.isJsonNull()) glucoseValue = jsonElement.getAsString();
	        else continue; // if the value is null, need not store that value
	        
	        // Support for High and low values
        	if (glucoseValue.toLowerCase().equals("HI".toLowerCase())) glucoseValue = "1000";
        	else if (glucoseValue.toLowerCase().equals("LO".toLowerCase())) glucoseValue = "0";
	        
	        
	        jsonElement = elementJsonObject.get("units");
	        if (!jsonElement.isJsonNull()) units = jsonElement.getAsString();
	        else units = "null";
	        
	        jsonElement = elementJsonObject.get("displayTime");
	        if (!jsonElement.isJsonNull()) displayTime = jsonElement.getAsString();
	        else displayTime = "2000-01-01T00:00:00.000Z";
	        
	        if (count == 0) writableData.append("(" 
	          + syncTimestamp + ", " 
	          + displayTime + ", " 
	          + glucoseValue + ", " 
	          + units + ", "
	          + systemTime + ")"); 
	        else writableData.append(", (" 
	          + syncTimestamp + ", " 
	          + displayTime + ", " 
	          + glucoseValue + ", " 
	          + units + ", "
	          + systemTime + ")");
	        count++;
	      }
	      return writableData.toString();
	    } catch (Exception e) {
	      log.error(e.getMessage());
	      return null;
	    }
	}
	
	
	private String processIPGlucoseNonManualData(String response) {
		// Initializing with empty string in case there is no data in the response
		StringBuffer writableData = new StringBuffer("");
		try {
			JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
			JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("pumpsReadings");
			int count = 0;
			for (JsonElement element : jsonArray) {
				String syncTimestamp, timestamp, value, type, units, timeOffset;
				
				JsonObject elementJsonObject = element.getAsJsonObject();
				
				JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
				if (!jsonElement.isJsonNull()) syncTimestamp = jsonElement.getAsString();
		        else syncTimestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("timestamp");
				if (!jsonElement.isJsonNull()) timestamp = jsonElement.getAsString();
		        else timestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("value");
				if (!jsonElement.isJsonNull()) value = jsonElement.getAsString();
		        else continue; // if the value is null, need not store that value
				
				// Support for High and low values
	        	if (value.toLowerCase().equals("HI".toLowerCase())) value = "1000";
	        	else if (value.toLowerCase().equals("LO".toLowerCase())) value = "0";
				
	        	jsonElement = elementJsonObject.get("type");
				if (!jsonElement.isJsonNull()) type = jsonElement.getAsString();
		        else type = "null";
				
				if (type.equals("manual")) continue; // store only non manual values
				
				
				jsonElement = elementJsonObject.get("units");
				if (!jsonElement.isJsonNull()) units = jsonElement.getAsString();
		        else units = "null";
				
				jsonElement = elementJsonObject.get("timeOffset");
				if (!jsonElement.isJsonNull()) timeOffset = jsonElement.getAsString();
		        else timeOffset = "2000-01-01T00:00:00.000Z";
				
				if (count == 0) writableData.append("(" 
					+ syncTimestamp + ", " 
					+ timestamp + ", " 
					+ value + ", " 
					+ units + ", "
					+ timeOffset+ ")"); 
				else writableData.append(", (" 
					+ syncTimestamp + ", " 
					+ timestamp + ", " 
					+ value + ", " 
					+ units + ", "
					+ timeOffset + ")");
				count++;
			}
			return writableData.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	
	@SuppressWarnings("unused")
	private String processFoodsAPIData(String response) {
		// Initializing with empty string in case there is no data in the response
		StringBuffer writableData = new StringBuffer("");
		try {
			JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
			JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("foods");
			int count = 0;
			for (JsonElement element : jsonArray) {
				String syncTimestamp, timestamp, carbs, updatedAt;
				JsonObject elementJsonObject = element.getAsJsonObject();
				
				JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
				if (!jsonElement.isJsonNull()) syncTimestamp = jsonElement.getAsString();
		        else syncTimestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("timestamp");
				if (!jsonElement.isJsonNull()) timestamp = jsonElement.getAsString();
		        else timestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("carbs");
				if (!jsonElement.isJsonNull()) carbs = jsonElement.getAsString();
		        else carbs = "0";
				
				jsonElement = elementJsonObject.get("updatedAt");
				if (!jsonElement.isJsonNull()) updatedAt = jsonElement.getAsString();
		        else updatedAt = "2000-01-01T00:00:00.000Z";
				
				// if carbs are 0, ignore them in data retrieval
				double carbsAsNumber;
				try {
					carbsAsNumber = Double.parseDouble(carbs);
				} catch (Exception e) {
					log.warn("Could not process carbs as a double");
					carbsAsNumber = 0;
				}
				if (carbsAsNumber <= 0) continue;
				
				if (count == 0) writableData.append("(" 
					+ syncTimestamp + ", " 
					+ timestamp + ", " 
					+ carbs + ", "
					+ updatedAt+ ")"); 
				else writableData.append(", (" 
					+ syncTimestamp + ", " 
					+ timestamp + ", " 
					+ carbs + ", " 
					+ updatedAt + ")");
				count++;
			}
			return writableData.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	private String processFoodsAPIDataWithTZAdjustedUpdatedAt(String response, 
															  String offsetInMinutes, 
															  String getFromTimeStamp, 
															  String getToTimeStamp) {
		long getFromTimeStampInEpoch = Instant.parse(getFromTimeStamp).toEpochMilli();
		long getToTimeStampInEpoch = Instant.parse(getToTimeStamp).toEpochMilli();
		
		// Initializing with empty string in case there is no data in the response
		StringBuffer writableData = new StringBuffer("");
		try {
			JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
			JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("foods");
			int count = 0;
			for (JsonElement element : jsonArray) {
				String syncTimestamp, timestamp, carbs, updatedAt;
				JsonObject elementJsonObject = element.getAsJsonObject();
				
				JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
				if (!jsonElement.isJsonNull()) syncTimestamp = jsonElement.getAsString();
		        else syncTimestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("timestamp");
				if (!jsonElement.isJsonNull()) timestamp = jsonElement.getAsString();
		        else timestamp = "2000-01-01T00:00:00.000Z";
				
				jsonElement = elementJsonObject.get("carbs");
				if (!jsonElement.isJsonNull()) carbs = jsonElement.getAsString();
		        else carbs = "0";
				
				jsonElement = elementJsonObject.get("updatedAt");
				if (!jsonElement.isJsonNull()) updatedAt = jsonElement.getAsString();
		        else updatedAt = "2000-01-01T00:00:00.000Z";
				
				// Updated at adjusted to the time zone
				Instant instant = Instant.parse(updatedAt);
				long dataTimeStampMilliAdjusted = instant.toEpochMilli() + Integer.parseInt(offsetInMinutes)*60*1000;
				String updatedAtAdjusted = Instant.ofEpochMilli(dataTimeStampMilliAdjusted).toString();
				
				// If the dataTimeStampMilliAdjusted is not between from and to timestamp, don't store
				
				if (dataTimeStampMilliAdjusted < getFromTimeStampInEpoch 
						|| dataTimeStampMilliAdjusted >= getToTimeStampInEpoch) continue;
				
				// if carbs are 0, ignore them in data retrieval
				double carbsAsNumber;
				try {
					carbsAsNumber = Double.parseDouble(carbs);
				} catch (Exception e) {
					log.warn("Could not process carbs as a double");
					carbsAsNumber = 0;
				}
				if (carbsAsNumber <= 0) continue;
				
				if (count == 0) writableData.append("(" 
					+ syncTimestamp + ", " 
					+ updatedAtAdjusted + ", " 
					+ carbs + ", "
					+ timestamp+ ")"); 
				else writableData.append(", (" 
					+ syncTimestamp + ", " 
					+ updatedAtAdjusted + ", " 
					+ carbs + ", " 
					+ timestamp + ")");
				count++;
			}
			return writableData.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	private String processIPNormalBolusesData(String response) {
	    // Initializing with empty string in case there is no data in the response
	    StringBuffer writableData = new StringBuffer("");
	    try {
	      JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
	      JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("pumpsNormalBoluses");
	      int count = 0;
	      for (JsonElement element : jsonArray) {
	    	String syncTimestamp, timestamp, carbs, updatedAt;
	        JsonObject elementJsonObject = element.getAsJsonObject();
	        
	        JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
	        if (!jsonElement.isJsonNull()) syncTimestamp = elementJsonObject.get("syncTimestamp").getAsString();
	        else syncTimestamp = "2000-01-01T00:00:00.000Z";
	        
	        jsonElement = elementJsonObject.get("timestamp");
	        if (!jsonElement.isJsonNull()) timestamp = elementJsonObject.get("timestamp").getAsString();
	        else timestamp = "2000-01-01T00:00:00.000Z";
	        
	        jsonElement = elementJsonObject.get("carbsInput");
	        if (!jsonElement.isJsonNull()) carbs = elementJsonObject.get("carbsInput").getAsString();
	        else continue; // if the carbs is null, need not store that value
	        
	        // replace comma with : in the carbs input
	        carbs = carbs.replace(',', ':');
	        
	        jsonElement = elementJsonObject.get("updatedAt");
	        if (!jsonElement.isJsonNull()) updatedAt = elementJsonObject.get("updatedAt").getAsString(); 
	        else updatedAt = "2000-01-01T00:00:00.000Z";
	        
	        // if all carbs are 0, ignore this element in data retrieval
			double carbsAsNumber;
			String [] carbTokens = carbs.split(":");
			boolean ignore = true;
			
			for (String carbToken: carbTokens) {
				try {
					carbsAsNumber = Double.parseDouble(carbToken);
				} catch (Exception e) {
					log.warn("Could not process carbs as a double");
					carbsAsNumber = 0;
				}
				if (carbsAsNumber > 0) {
					ignore = false;
					break;
				}
			}
			if (ignore) continue;
	        
	        if (count == 0) writableData.append("(" 
				+ syncTimestamp + ", " 
				+ timestamp + ", " 
				+ carbs + ", "
				+ updatedAt+ ")"); 
			else writableData.append(", (" 
				+ syncTimestamp + ", " 
				+ timestamp + ", " 
				+ carbs + ", " 
				+ updatedAt + ")");
	        count++;
	      }
	      return writableData.toString();
	    } catch (Exception e) {
	      log.error(e.getMessage());
	      return null;
	    }
	}
	
	
	private String processIPExtendedBolusesData(String response) {
	    // Initializing with empty string in case there is no data in the response
	    StringBuffer writableData = new StringBuffer("");
	    try {
	      JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
	      JsonArray jsonArray = jsonObjectResponse.getAsJsonArray("pumpsExtendedBoluses");
	      int count = 0;
	      for (JsonElement element : jsonArray) {
	    	String syncTimestamp, timestamp, carbs, updatedAt;
	        JsonObject elementJsonObject = element.getAsJsonObject();
	        
	        JsonElement jsonElement = elementJsonObject.get("syncTimestamp");
	        if (!jsonElement.isJsonNull()) syncTimestamp = elementJsonObject.get("syncTimestamp").getAsString();
	        else syncTimestamp = "2000-01-01T00:00:00.000Z";
	        
	        jsonElement = elementJsonObject.get("timestamp");
	        if (!jsonElement.isJsonNull()) timestamp = elementJsonObject.get("timestamp").getAsString();
	        else timestamp = "2000-01-01T00:00:00.000Z";
	        
	        jsonElement = elementJsonObject.get("carbsInput");
	        if (!jsonElement.isJsonNull()) carbs = elementJsonObject.get("carbsInput").getAsString();
	        else continue; // if the carbs is null, need not store that value
	        
	        // replace comma with : in the carbs input
	        carbs = carbs.replace(',', ':');
	        
	        jsonElement = elementJsonObject.get("updatedAt");
	        if (!jsonElement.isJsonNull()) updatedAt = elementJsonObject.get("updatedAt").getAsString(); 
	        else updatedAt = "2000-01-01T00:00:00.000Z";
	        
	        // if all carbs are 0, ignore this element in data retrieval
 			double carbsAsNumber;
 			String [] carbTokens = carbs.split(":");
 			boolean ignore = true;
 			
 			for (String carbToken: carbTokens) {
 				try {
 					carbsAsNumber = Double.parseDouble(carbToken);
 				} catch (Exception e) {
 					log.warn("Could not process carbs as a double");
 					carbsAsNumber = 0;
 				}
 				if (carbsAsNumber > 0) {
 					ignore = false;
 					break;
 				}
 			}
 			if (ignore) continue;
	        
	        if (count == 0) writableData.append("(" 
				+ syncTimestamp + ", " 
				+ timestamp + ", " 
				+ carbs + ", "
				+ updatedAt+ ")"); 
			else writableData.append(", (" 
				+ syncTimestamp + ", " 
				+ timestamp + ", " 
				+ carbs + ", " 
				+ updatedAt + ")");
	        count++;
	      }
	      return writableData.toString();
	    } catch (Exception e) {
	      log.error(e.getMessage());
	      return null;
	    }
	}
	
	
	private String getWearTimeFromCGMStatistics(String response) {
	    try {
	      JsonObject jsonObjectResponse = (JsonObject) new JsonParser().parse(response);
	      if (jsonObjectResponse.isJsonNull()) return "0";
	      
	      JsonObject jsonObjectCGMStatistics = jsonObjectResponse.getAsJsonObject("cgmStatistics");
	      if (jsonObjectCGMStatistics.isJsonNull()) return "0";
	      
	      JsonObject jsonObjectResults = jsonObjectCGMStatistics.getAsJsonObject("results");
	      if (jsonObjectResults.isJsonNull()) return "0";
	      
	      JsonObject jsonObjectActiveTime = jsonObjectResults.getAsJsonObject("activeTime");
	      if (jsonObjectActiveTime.isJsonNull()) return "0";
	      
	      JsonElement jsonObjectActiveTimeValue = jsonObjectActiveTime.get("value");
	      if (jsonObjectActiveTimeValue.isJsonNull()) return "0";
	      
	      
	      String activeTimeAsString = jsonObjectActiveTimeValue.getAsString();
	      int activeTime = 0;
	      if(!activeTimeAsString.contentEquals("-")) {
	    	  activeTime = jsonObjectActiveTimeValue.getAsInt();
	      }
	      
	      return activeTime + "";
	    } catch (Exception e) {
	      log.error(e.getMessage());
	      // When there is an error, 0 as string is returned. 
	      return "0";
	    }
	}
	
	private void writeDataToVariable(String writableData, ObjectId participantId, AbstractRule rule) {
		String storeVariableName = "";
		try {
			if(rule instanceof AbstractMonitoringRule) 
				storeVariableName = ((AbstractMonitoringRule) rule).getStoreValueToVariableWithName();
			else if (rule instanceof ScreeningSurveySlideRule)
				storeVariableName = ((ScreeningSurveySlideRule) rule).getStoreValueToVariableWithName();
			else {
				log.error("Rule is neither AbstractMonitoringRule nor ScreeningSurveySlideRule");
			}
			
			// Writing into the respective variable
			if(!StringUtils.isBlank(storeVariableName)) {
				variablesManagerService.writeVariableValueOfParticipant(
						participantId,
						storeVariableName,
						writableData);
			}
			
		} catch (Exception e) {
			log.error("Could not write variable value: {}", e.getMessage());
		}

	}

	public boolean bgmCheckIfSyncedInThisPeriod(RuleEvaluationResult ruleEvaluationResult) {	
		String [] timeStampTokens;
		String fromTimeStamp, toTimeStamp;
		List <String> syncTimeStamps  = new ArrayList <String> ();
		
		try {
			timeStampTokens = ruleEvaluationResult.getTextRuleValue().split(",");
			// TODO check if the rule evaluation result has correct format...
				
			fromTimeStamp = timeStampTokens[0].trim() + "T00:00:00.000Z";
			toTimeStamp = timeStampTokens[1].trim() + "T00:00:00.000Z";
			
			// 0 is where the Sync Timestamps are
			syncTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleComparisonTermValue().trim(), 0);
			
			if(syncTimeStamps == null) {
				log.info("Could not process the glucose data of the 'BGM synced data in this period' rule:");
				return false;
			}
			
			for (String syncTimeStamp: syncTimeStamps) {
				int compareFrom = syncTimeStamp.compareTo(fromTimeStamp);
				int compareTo = syncTimeStamp.compareTo(toTimeStamp);

				if(compareFrom >= 0 && compareTo < 0) {
					log.info("'BGM synced data in this period' rule: BGM synced at this given period.");
					return true;
				}
			}
			
		} catch (Exception e) {
			log.info("Could not process the input of the 'BGM synced data in this period' rule:", e.getMessage());
		}
		
		return false;
	}

	public boolean bgmSGGMDailyGoalAchieved(RuleEvaluationResult ruleEvaluationResult) {
		List <String> glucoseDataTimeStamps  = new ArrayList <String> ();
		List <Long> dataTimeStampsMilli  = new ArrayList <Long> ();
		try {
			// 1 is where the Timestamps are for both CGM (displayTime) and BGM 
			glucoseDataTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleValue().trim(), 1);
			
			if (glucoseDataTimeStamps == null) {
				log.info("Could not process the 'BGM daily goal achieved' rule:");
				return false;
			}
			
			dataTimeStampsMilli = convertISO8604ToEpoch(glucoseDataTimeStamps);
			
			if (dataTimeStampsMilli == null) {
				log.info("Could not convert the timestamps of the 'BGM daily goal achieved' rule:");
				return false;
			}
			
			if (dataTimeStampsMilli.isEmpty()) {
				log.info("Timestamps of the 'BGM daily goal achieved' rule are empty:");
				return false;
			}
			
			// Find if there are at least 5 timestamps with 2 hours difference
			return timestampsWithOffsetPresent(dataTimeStampsMilli, 5, 2*3600*1000);
			
		} catch (Exception e) {
			log.warn("Could not process the input of the 'BGM GM daily goal achieved' rule:", e.getMessage());
		}
		
		return false;
	}

	private boolean timestampsWithOffsetPresent(List<Long> dataTimeStampsMilli, int count, long offset) {
		if (dataTimeStampsMilli == null) return false;
		
		int length = dataTimeStampsMilli.size();
		
		if (length <= 0) return false;
		
		int cache [] [] = new int [length+1][count+1];
		
		// initializing cache
		for (int i = 0; i <= length; i++) {
			for (int j = 0; j <= count; j++) {
				cache [i][j] = -1;
			}
		}
		
		// Sorting dataTimeStampsMilli
		Collections.sort(dataTimeStampsMilli);
		recursiveCounter = 0;
		return rescursiveIfTmestampsWithOffsetPresent(dataTimeStampsMilli, 0, count, offset, cache);
	}


	private boolean rescursiveIfTmestampsWithOffsetPresent(
			List<Long> dataTimeStampsMilli, 
			int pos, int count, 
			long offset, 
			int[][] cache) {
		
		recursiveCounter++;
		
		if (recursiveCounter >= 1000) {
			log.error("Recursion from rescursiveIfTmestampsWithOffsetPresent is set to break as is it not stopping.");
			return false;
		}
		
		
		if (cache[pos][count] == 1) return true;
		if (cache[pos][count] == 0) return false;
		
		int length = dataTimeStampsMilli.size();
		
		if (count == 0) {
			cache [pos][count] = 1;
			return true;
		}
		
		if (pos + count > length) {
			cache [pos][count] = 0;
			return false;
		}
		
		for (int i = pos+1; i <= length; i++) {
			if (pos != 0 && (dataTimeStampsMilli.get(i-1) - dataTimeStampsMilli.get(pos-1) <= offset)) continue;
			else if (rescursiveIfTmestampsWithOffsetPresent(dataTimeStampsMilli, i, count-1, offset, cache)) {
				cache [pos][count] = 1;
				return true;
			}
		}
		
		cache[pos][count] = 0;
		return false;
	}
	
	public void bgmSGValidGlucoseCounts(RuleEvaluationResult ruleEvaluationResult) {
		try {
			List <String> glucoseDataTimeStamps  = new ArrayList <String> ();
			List <Long> dataTimeStampsMilli  = new ArrayList <Long> ();
			
			glucoseDataTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleValue().trim(), 1);
			
			if (glucoseDataTimeStamps == null) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.warn("Could not process the 'BGM valid glucose counts' rule:");
				return;
			}
			
			dataTimeStampsMilli = convertISO8604ToEpoch(glucoseDataTimeStamps);
			
			if (dataTimeStampsMilli == null) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.warn("Could not convert the timestamps of the 'BGM valid glucose counts' rule:");
				return;
			}
			
			if (dataTimeStampsMilli.isEmpty()) {
				log.warn("Timestamps of the 'BGM valid glucose counts' rule are empty:");
				ruleEvaluationResult.setTextRuleValue("0");
				return;
			}
			
			Collections.sort(dataTimeStampsMilli);
			
			int countValidGlucoseEntries = 0; 
			long lastCountedTimeStamp = 0;
			
			for (int i = 0; i < dataTimeStampsMilli.size(); i++) {
				long currentTimeStamp = dataTimeStampsMilli.get(i);
				// Timestamp - remove seconds and milliseconds
				if (timestampWithSecondsAndMillisecondsRemoved(currentTimeStamp)
						- timestampWithSecondsAndMillisecondsRemoved(lastCountedTimeStamp) >= 2 * 60 * 60 * 1000) {
					lastCountedTimeStamp = currentTimeStamp;
					countValidGlucoseEntries++;
				}
			}
			log.info("The number of valid glucose entries: " + countValidGlucoseEntries);
			ruleEvaluationResult.setTextRuleValue("" + countValidGlucoseEntries);
			
		}
		catch (Exception e) {
			ruleEvaluationResult.setTextRuleValue("0");
			log.warn("Could not process the input of the 'BGM valid glucose counts' rule:", e.getMessage());
		}
	}
	
	public void getCountSGMealtimeBehaviourValidPairs(RuleEvaluationResult ruleEvaluationResult) {
		List <String> glucoseDataTimeStamps  = new ArrayList <String> ();
		List <Long> glucoseDataTimeStampsEpoch  = new ArrayList <Long> ();
		List <String> carbsDataTimeStamps  = new ArrayList <String> ();
		List <Long> carbsDataTimeStampsEpoch  = new ArrayList <Long> ();
		
		try {
		
			// 1 is the position where the Timestamps are
			glucoseDataTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleValue().trim(), 1);
			carbsDataTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleComparisonTermValue().trim(), 1);
			
			if (glucoseDataTimeStamps == null) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.warn("Could not get the glucose timestamps.");
				return;
			}
			
			if (carbsDataTimeStamps == null) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.warn("Could not get the carbs timestamps.");
				return;
			}
			
			glucoseDataTimeStampsEpoch = convertISO8604ToEpoch(glucoseDataTimeStamps);
			carbsDataTimeStampsEpoch = convertISO8604ToEpoch(carbsDataTimeStamps);
			
			if (glucoseDataTimeStampsEpoch == null) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.warn("Could not convert the timestamps to unix timestamps of the glucose data.");
				return;
			}
			
			if (carbsDataTimeStampsEpoch == null) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.warn("Could not convert the timestamps to unix timestamps of the carbs data.");
				return;
			}
			
			if (glucoseDataTimeStampsEpoch.isEmpty()) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.info("Timestamps of the glucose data are empty.");
				return;
			}
			
			if (carbsDataTimeStampsEpoch.isEmpty()) {
				ruleEvaluationResult.setTextRuleValue("0");
				log.info("Timestamps of the carbs data are empty.");
				return;
			}
			
			int countValidCarbEntries = 0; 
			long carbsLastValidDataTimeStampEpoch = 0;
			
			Collections.sort(carbsDataTimeStampsEpoch);
			Collections.sort(glucoseDataTimeStampsEpoch);
			
			for (int i=0; i < carbsDataTimeStampsEpoch.size(); i++) {
				long carbsDataTimeStampEpoch = carbsDataTimeStampsEpoch.get(i);
				
				// 2 hour offset between each entry
				// Timestamp - remove seconds and milliseconds
				if (timestampWithSecondsAndMillisecondsRemoved(carbsDataTimeStampEpoch) - 
						timestampWithSecondsAndMillisecondsRemoved(carbsLastValidDataTimeStampEpoch) < 2 * 60 * 60 * 1000)
					continue;
				
				for (int j=0; j < glucoseDataTimeStampsEpoch.size(); j++) {
					long glucoseDataTimeStampEpoch = glucoseDataTimeStampsEpoch.get(j);
					
					// Timestamp - remove seconds and milliseconds
					long difference = timestampWithSecondsAndMillisecondsRemoved(glucoseDataTimeStampEpoch) 
							- timestampWithSecondsAndMillisecondsRemoved(carbsDataTimeStampEpoch);
					
					if (difference > 5 * 60 * 1000) break;
					
					// -35 to +5 minutes
					if (difference >= -35 * 60 * 1000 && difference <= 5 * 60 * 1000) {
						carbsLastValidDataTimeStampEpoch = carbsDataTimeStampEpoch;
						countValidCarbEntries++;
						break;
					}
				}
			}
			log.info("The number of valid carb entries with a matching glucose entry: " + countValidCarbEntries);
			ruleEvaluationResult.setTextRuleValue("" + countValidCarbEntries);
			
		} 
		catch (Exception e) {
			ruleEvaluationResult.setTextRuleValue("0");
			log.warn("Could not process the input of the 'MB count valid pairs' rule:", e.getMessage());
		}
	}
	
	public boolean glucoseDataContainsAboveOrEqualToGivenValue(RuleEvaluationResult ruleEvaluationResult) {
		try {
			List <String> glucoseValues  = new ArrayList <String> ();
			// Glucose values are at the position 2
			glucoseValues = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleValue().trim(), 2);
			String comparingGlucoseValueString = ruleEvaluationResult.getTextRuleComparisonTermValue().trim();
			double comparingGlucoseValue = Double.parseDouble(comparingGlucoseValueString);
			
			if (glucoseValues == null) {
				log.warn("Could not get the glucose values.");
				return false;
			}
			
			if (glucoseValues.isEmpty()) {
				log.debug("Empty glucose value is given as input.");
				return false;
			}
			
			for (int i = 0; i < glucoseValues.size(); i++) {
				String glucoseValueString = glucoseValues.get(i);
				double glucoseValue = Double.parseDouble(glucoseValueString);
				
				if (glucoseValue >= comparingGlucoseValue) return true;
				
			}
			
			return false;
		}
		catch (Exception e) {
			log.warn("Could not process the input of the 'glucose Data contains above or equal given value' rule:", e.getMessage());
			return false;
		}
	}
	
	public boolean glucoseDataContainsBelowGivenValue(RuleEvaluationResult ruleEvaluationResult) {
		try {
			List <String> glucoseValues  = new ArrayList <String> ();
			// Glucose values are at the position 2
			glucoseValues = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleValue().trim(), 2);
			String comparingGlucoseValueString = ruleEvaluationResult.getTextRuleComparisonTermValue().trim();
			double comparingGlucoseValue = Double.parseDouble(comparingGlucoseValueString);
			
			if (glucoseValues == null) {
				log.warn("Could not get the glucose values.");
				return false;
			}
			
			if (glucoseValues.isEmpty()) {
				log.debug("Empty glucose value is given as input.");
				return false;
			}
			
			for (int i = 0; i < glucoseValues.size(); i++) {
				String glucoseValueString = glucoseValues.get(i);
				double glucoseValue = Double.parseDouble(glucoseValueString);
				
				if (glucoseValue < comparingGlucoseValue) return true;
				
			}
			
			return false;
		}
		catch (Exception e) {
			log.warn("Could not process the input of the 'glucose Data contains below given value' rule:", e.getMessage());
			return false;
		}
	}
	
	// Not used.
	public boolean bgmSGMBDailyGoalAchieved(RuleEvaluationResult ruleEvaluationResult) {
		List <String> glucoseDataTimeStamps  = new ArrayList <String> ();
		List <Long> glucoseDataTimeStampsEpoch  = new ArrayList <Long> ();
		List <String> carbsDataTimeStamps  = new ArrayList <String> ();
		List <Long> carbsDataTimeStampsEpoch  = new ArrayList <Long> ();
		
		try {
		
			// 1 is the position where the Timestamps are
			glucoseDataTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleValue().trim(), 1);
			carbsDataTimeStamps = getValuesAtPositionFromData(ruleEvaluationResult.getTextRuleComparisonTermValue().trim(), 1);
			
			if (glucoseDataTimeStamps == null) {
				log.info("Could not get the glucose timestamps.");
				return false;
			}
			
			if (carbsDataTimeStamps == null) {
				log.info("Could not get the carbs timestamps.");
				return false;
			}
			
			glucoseDataTimeStampsEpoch = convertISO8604ToEpoch(glucoseDataTimeStamps);
			carbsDataTimeStampsEpoch = convertISO8604ToEpoch(carbsDataTimeStamps);
			
			if (glucoseDataTimeStampsEpoch == null) {
				log.info("Could not convert the timestamps to unix timestamps of the glucose data.");
				return false;
			}
			
			if (carbsDataTimeStampsEpoch == null) {
				log.info("Could not convert the timestamps to unix timestamps of the carbs data. ");
				return false;
			}
			
			if (glucoseDataTimeStampsEpoch.isEmpty()) {
				log.info("Timestamps of the glucose data are empty.");
				return false;
			}
			
			if (carbsDataTimeStampsEpoch.isEmpty()) {
				log.info("Timestamps of the carbs data are empty.");
				return false;
			}
			
			int countValidCarbEntries = 0; 
			
			Collections.sort(carbsDataTimeStampsEpoch);
			Collections.sort(glucoseDataTimeStampsEpoch);
			
			for (int i=0; i < carbsDataTimeStampsEpoch.size(); i++) {
				long carbsDataTimeStampEpoch = carbsDataTimeStampsEpoch.get(i);
				for (int j=0; j < glucoseDataTimeStampsEpoch.size(); j++) {
					long glucoseDataTimeStampEpoch = glucoseDataTimeStampsEpoch.get(j);
					
					// Timestamp - remove seconds and milliseconds
					if (timestampWithSecondsAndMillisecondsRemoved(carbsDataTimeStampEpoch) < 
							timestampWithSecondsAndMillisecondsRemoved(glucoseDataTimeStampEpoch)) break;
					
					// Timestamp - remove seconds and milliseconds
					if (timestampWithSecondsAndMillisecondsRemoved(carbsDataTimeStampEpoch) - 
							timestampWithSecondsAndMillisecondsRemoved(glucoseDataTimeStampEpoch) <= 30 * 60 * 1000) {
						countValidCarbEntries++;
						break;
					}
				}
				if (countValidCarbEntries >= 3) return true;
			}
			
		} 
		catch (Exception e) {
			log.info("Could not process the input of the 'BGM MB daily goal achieved' rule:", e.getMessage());
		}
		return false;
	}


	private List<Long> convertISO8604ToEpoch(List<String> iSO8604TimeStamps) {
		if (iSO8604TimeStamps == null) return null;
		List <Long> dataTimeStampsMilli  = new ArrayList <Long> ();
		
		for (String iSO8604TimeStamp: iSO8604TimeStamps) {
			Instant instant = Instant.parse(iSO8604TimeStamp);
			long dataTimeStampMilli = instant.toEpochMilli();
			dataTimeStampsMilli.add(dataTimeStampMilli);
		}
		
		return dataTimeStampsMilli;
	}
	


	private List<String> getValuesAtPositionFromData(String data, int position) {
		
		if (position < 0) {
			log.error("Method getValuesAtPositionFromData called with position is set less than 0");
			return null;
		}
		
		List <String> values = new ArrayList <String>();
		List <StringBuilder> dataPoints = new ArrayList <StringBuilder>();
		StringBuilder dataPoint = null;  
		boolean add = false;
		
		for (int i=0; i < data.length(); i++) {
			char characterFromData = data.charAt(i);
			if(characterFromData == '(') {
				 dataPoint = new StringBuilder();
				 add = true;
			}
			else if (characterFromData == ')') {
				 dataPoints.add(dataPoint);
				 add = false;
			}
			else {
				if (add && dataPoint != null) {
					dataPoint.append(characterFromData);
				}
			}	
		}
		
		for (StringBuilder dp: dataPoints) {
			String dpString = dp.toString();
			String[] all = dpString.split(",");
			if (position < all.length) {
				values.add(all[position].trim());
			}
		}
		
		return values;
	}
	
}

package ch.ethz.mc.services.threads;

import java.util.concurrent.TimeUnit;

import ch.ethz.mc.conf.ImplementationConstants;
import ch.ethz.mc.model.memory.SystemLoad;
import ch.ethz.mc.services.CTPayerManagerService;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * Manages the CTPayer requests
 * 
 * @author Prabhakaran Santhanam
 */
@Log4j2
public class CTPayerWorker extends Thread {
	private final SystemLoad							systemLoad;
	
	private CTPayerManagerService 						cTPayerManagerService;
	@Setter
	private boolean										shouldStop	= false;

	public CTPayerWorker(CTPayerManagerService cTPayerManagerService) {
		setName("CTPayerDataWorker");
		setPriority(NORM_PRIORITY - 1);
		
		systemLoad = SystemLoad.getInstance();
		this.cTPayerManagerService = cTPayerManagerService;
	}
	
	@Override
	public void run() {
		try {
			TimeUnit.MILLISECONDS.sleep(
					ImplementationConstants.CTPAYER_WORKER_MILLISECONDS_SLEEP_BETWEEN_CHECK_CYCLES);
		} catch (final InterruptedException e) {
			interrupt();
			log.debug(
					"CTPayer worker received signal to stop (before first run)");
		}

		while (!isInterrupted() && !shouldStop) {
			final long startingTime = System.currentTimeMillis();
			log.debug("Executing new run of CTPayer worker...started");

			try {
				cTPayerManagerService.handleCTPayerRequests();
			} catch (final Exception e) {
				log.error("Could not perform all requests: {}",
						e.getMessage());
			}

			
			systemLoad.setCTPayerWorkerRequiredMillis(
					System.currentTimeMillis() - startingTime);
			log.debug(
					"Executing new run of CTPayer worker...done ({} milliseconds)",
					System.currentTimeMillis() - startingTime);

			try {
				TimeUnit.MILLISECONDS.sleep(
						ImplementationConstants.CTPAYER_WORKER_MILLISECONDS_SLEEP_BETWEEN_CHECK_CYCLES);
			} catch (final InterruptedException e) {
				interrupt();
				return;
			}
		}
		log.debug("CTPayer worker received signal to stop");
	}

}

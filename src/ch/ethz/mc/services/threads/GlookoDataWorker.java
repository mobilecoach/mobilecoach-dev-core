package ch.ethz.mc.services.threads;

import java.util.concurrent.TimeUnit;

import ch.ethz.mc.conf.ImplementationConstants;
import ch.ethz.mc.model.memory.SystemLoad;
import ch.ethz.mc.services.GlookoDataManagerService;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * Manages the Glooko requests
 * 
 * @author Prabhakaran Santhanam
 */
@Log4j2
public class GlookoDataWorker extends Thread {
	private final SystemLoad							systemLoad;
	
	private GlookoDataManagerService 					glookoDataManagerService;
	@Setter
	private boolean										shouldStop	= false;

	public GlookoDataWorker(GlookoDataManagerService glookoDataManagerService) {
		setName("GlookoDataWorker");
		setPriority(NORM_PRIORITY - 1);
		
		systemLoad = SystemLoad.getInstance();
		this.glookoDataManagerService = glookoDataManagerService;
	}
	
	@Override
	public void run() {
		try {
			TimeUnit.MILLISECONDS.sleep(
					ImplementationConstants.GLOOKO_DATA_WORKER_MILLISECONDS_SLEEP_BETWEEN_CHECK_CYCLES);
		} catch (final InterruptedException e) {
			interrupt();
			log.debug(
					"Glooko data worker received signal to stop (before first run)");
		}

		while (!isInterrupted() && !shouldStop) {
			final long startingTime = System.currentTimeMillis();
			log.debug("Executing new run of glooko data worker...started");

			try {
				glookoDataManagerService.handleGlookoRequests();
			} catch (final Exception e) {
				log.error("Could not perform all requests: {}",
						e.getMessage());
			}

			
			systemLoad.setGlookoDataWorkerRequiredMillis(
					System.currentTimeMillis() - startingTime);
			log.debug(
					"Executing new run of glooko data worker...done ({} milliseconds)",
					System.currentTimeMillis() - startingTime);

			try {
				TimeUnit.MILLISECONDS.sleep(
						ImplementationConstants.GLOOKO_DATA_WORKER_MILLISECONDS_SLEEP_BETWEEN_CHECK_CYCLES);
			} catch (final InterruptedException e) {
				interrupt();
				return;
			}
		}
		log.debug("Glooko data worker received signal to stop");
	}

}

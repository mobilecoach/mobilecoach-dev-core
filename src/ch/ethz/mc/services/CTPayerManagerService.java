package ch.ethz.mc.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import ch.ethz.mc.conf.Constants;
import ch.ethz.mc.model.memory.RuleEvaluationResult;
import ch.ethz.mc.model.persistent.ScreeningSurveySlideRule;
import ch.ethz.mc.model.persistent.concepts.AbstractMonitoringRule;
import ch.ethz.mc.model.persistent.concepts.AbstractRule;
import ch.ethz.mc.services.internal.VariablesManagerService;
import ch.ethz.mc.services.threads.CTPayerWorker;
import ch.ethz.mc.tools.RuleEvaluator;
import lombok.extern.log4j.Log4j2;


/**
 * Cares for requests to the CTPayer server. 
 *
 * @author Prabhakaran Santhanam
 */
@Log4j2
public class CTPayerManagerService {
	
	private static CTPayerManagerService instance				    = null;
	
	private final VariablesManagerService	variablesManagerService;
	private boolean 						cTPayerIntegrationActive;
	private String 							cTPayerClientUsername;
	private String							cTPayerClientPassword;
	private String							cTPayerAPIUsername;
	private String							cTPayerAPIURL;
	
	private final CTPayerWorker				cTPayerWorker;
	private final Queue <CTPayerRequest> 	requestQueue;
	
	private CTPayerManagerService(
			final VariablesManagerService variablesManagerService)
				throws Exception {
		log.info("Starting CTPayer service...");
		
		this.variablesManagerService = variablesManagerService;
		this.cTPayerIntegrationActive = Constants.isCTPayerIntegrationActive();
		this.cTPayerClientUsername = Constants.getCTPayerClientUsername();
		this.cTPayerClientPassword = Constants.getCTPayerClientPassword();
		this.cTPayerAPIUsername = Constants.getCTPayerAPIUsername();
		this.cTPayerAPIURL = Constants.getCTPayerAPIURL();
		
		cTPayerWorker = new CTPayerWorker(this);
		requestQueue = new LinkedList<CTPayerRequest>();
		
		log.info("CTPayer service started.");
	}
	
	
	public static CTPayerManagerService start(
			final VariablesManagerService variablesManagerService)
			throws Exception {
		if (instance == null) {
			instance = new CTPayerManagerService(variablesManagerService);
		}
	
		RuleEvaluator.setCTPayerManagerService(instance);
		return instance;
	}
	
	public void startWorking() {
		new Thread("CTPayer Worker Starter") {
			@Override
			public void run() {
				log.info("Starting CTPayer worker...");

				// Start CTPayer manager service
				try {
					cTPayerWorker.start();
				} catch (final Exception e) {
					log.error("Error at starting CTPayer worker: {}",
							e.getMessage());
				}

				log.info("Started.");
			}
		}.start();
	}
	
	public void stop() throws Exception {
		log.info(
				"Stopping CTPayer worker and and service (takes several seconds)...");
		synchronized (cTPayerWorker) {
			cTPayerWorker.setShouldStop(true);
			Thread.sleep(2000);
			try {
				cTPayerWorker.interrupt();
				cTPayerWorker.join();
			} catch (final Exception e) {
				// Do nothing
			}
		}
		log.info("Stopped.");
	}
	
	public void generateRequest(
			final ObjectId participantId, 
			final AbstractRule rule, 
			final RuleEvaluationResult ruleEvaluationResult) {
		log.debug("Generating a new request...");
		CTPayerRequest newRequest = new CTPayerRequest(participantId, rule, ruleEvaluationResult);
		// TODO: find out why you need a synchronized over requestQueue 
		synchronized (requestQueue) {
			requestQueue.add(newRequest);	
		}
	}
	
	/**
	 *
	 * @author Prabhakaran Santhanam
	 */
	private class CTPayerRequest {
		private final ObjectId participantId;
		private final AbstractRule rule; 
		private final RuleEvaluationResult ruleEvaluationResult;
		
		CTPayerRequest (
			final ObjectId participantId, 
			final AbstractRule rule, 
			final RuleEvaluationResult ruleEvaluationResult) {
			
			this.participantId = participantId;
			this.rule = rule;
			this.ruleEvaluationResult = ruleEvaluationResult;
		}
	}

	
	public void handleCTPayerRequests() {
		while(!requestQueue.isEmpty()) {
			CTPayerRequest currentRequest = requestQueue.poll();
			
			if (!cTPayerIntegrationActive) {
				log.warn("The CTPayer service is disabled in the configuration file. Hence the requests are ignored.");
				continue;
			}
			
			if(currentRequest!=null) {	
				String [] requestInfoTokens;
				String cardId, date, internalPaymentId, description, amount, body;
				switch (currentRequest.rule.getRuleEquationSign()) {
					case CTPAYER_ASYNC_PAY_PARTICIPANT_AND_GET_STATUS_BUT_RESULT_ALWAYS_TRUE:
						try {
							requestInfoTokens = 
									currentRequest.ruleEvaluationResult.getTextRuleValue().split(",");
							cardId = requestInfoTokens[0].trim();
							date = requestInfoTokens[1].trim();
							internalPaymentId = requestInfoTokens[2].trim();
							description = requestInfoTokens[3].trim();
							amount = currentRequest.ruleEvaluationResult.getTextRuleComparisonTermValue();
							
							date = URLEncoder.encode(date, "utf-8");
							description = URLEncoder.encode(description, "utf-8");
							
							body = "username=" + cTPayerClientUsername + "&" +
									"password=" + cTPayerClientPassword + "&" + 
									"cardId=" + cardId + "&" +
									"internalPaymentId=" + internalPaymentId + "&" +
									"amount=" + amount + "&" + 
									"description=" + description + "&" + 
									"paymentDate=" + date  + "&" + 
									"apiusername=" + cTPayerAPIUsername;
							
							URL url = new URL(cTPayerAPIURL + "/loadcard.php?");
							
							log.info("The request URL is: " + url.toString());
							log.info("The request body is: " + body);
							
							HttpURLConnection con = (HttpURLConnection) url.openConnection();
							con.setRequestMethod("POST");
							con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
							
							con.setDoOutput(true);
							DataOutputStream out = new DataOutputStream(con.getOutputStream());
							out.writeBytes(body);
							out.flush();
							out.close();
							
							int status = con.getResponseCode();
							BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
							String inputLine;
							StringBuffer content = new StringBuffer();
							while((inputLine = in.readLine()) != null) {
							  content.append(inputLine);
							}
							in.close();
							con.disconnect();
							
							// Write the status in a variable
							writeDataToVariable(
									status + ": " + content, 
									currentRequest.participantId, 
									currentRequest.rule);
						}
						catch(Exception e) {
							log.error("Error occurred whle processing rule (CTPayer async - pay participant): " +e.getMessage());
							writeDataToVariable(
									"Error:" + System.currentTimeMillis(), 
									currentRequest.participantId, 
									currentRequest.rule);
						}
						break;
					
					default:
						log.warn("Unknown rule.");
						writeDataToVariable(
								"Error:" + System.currentTimeMillis(), 
								currentRequest.participantId, 
								currentRequest.rule);
						break;
				}
			}
		}
	}
	
	
	private void writeDataToVariable(String writableData, ObjectId participantId, AbstractRule rule) {
		String storeVariableName = "";
		try {
			if(rule instanceof AbstractMonitoringRule) 
				storeVariableName = ((AbstractMonitoringRule) rule).getStoreValueToVariableWithName();
			else if (rule instanceof ScreeningSurveySlideRule)
				storeVariableName = ((ScreeningSurveySlideRule) rule).getStoreValueToVariableWithName();
			else {
				log.error("Rule is neither AbstractMonitoringRule nor ScreeningSurveySlideRule");
			}
			
			// Writing into the respective variable
			if(!StringUtils.isBlank(storeVariableName)) {
				variablesManagerService.writeVariableValueOfParticipant(
						participantId,
						storeVariableName,
						writableData);
			}
			
		} catch (Exception e) {
			log.error("Could not write variable value: {}", e.getMessage());
		}

	}

}

package ch.ethz.mc.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ch.ethz.mc.MC;
import ch.ethz.mc.conf.Constants;
import ch.ethz.mc.model.persistent.GlookoCode;
import ch.ethz.mc.model.persistent.GlookoFoodsAPINotification;
import ch.ethz.mc.services.InterventionAdministrationManagerService;
import lombok.val;
import lombok.extern.log4j.Log4j2;

/**
 * Servlet implementation class GlookoNotification
 * 
 * @author Prabhakaran Santhanam
 */

@WebServlet(displayName = "GlookoNotification", urlPatterns = "/glooko/notification/*", asyncSupported = true, loadOnStartup = 1)
@Log4j2
public class GlookoNotification extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static InterventionAdministrationManagerService interventionAdministrationManagerService = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GlookoNotification() {
        super();
    }
    
    @Override
    public void init(final ServletConfig servletConfig)
			throws ServletException {
    	if (!MC.getInstance().isReady()) {
			log.error("Servlet {} can't be started. Context is not ready!",
					this.getClass());
			throw new ServletException("Context is not ready!");
		}

    	if (Constants.isGlookoIntegrationActive()) {
	    	interventionAdministrationManagerService = MC.getInstance().getInterventionAdministrationManagerService();
			log.info("Initializing (Glooko notification) servlet...");
			super.init(servletConfig);
    	} else log.info("Glooko notification servlet not required.");
    	
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logRequest(request);
		response.getWriter().append("Glooko's notification url.");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * TODO improve this so that it is robust. 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logRequest(request); // Only the headers
		val bodyOfRequest = getBody(request);
		log.debug("Body of request: " + bodyOfRequest);
		
		if(!bodyOfRequest.equals("")) {
			try {
				JsonObject jsonObject = (JsonObject) new JsonParser().parse(bodyOfRequest);
				// Check for new user notification
				if(jsonObject.get("user_id") != null && jsonObject.get("glookoCode") == null) {
					val created 		= new Date();
					String firstName = "", lastName = "", dateOfBirth = "", glookoCodeS = "", email = "", mrn = ""; 
					
				    val firstNameElement = jsonObject.get("first_name");
				    if(firstNameElement != null) firstName 	= firstNameElement.getAsString();
				    
				    val lastNameElement = jsonObject.get("last_name");
				    if(lastNameElement != null) lastName = lastNameElement.getAsString();
				    
				    val dateOfBirthElement = jsonObject.get("date_of_birth");
				    if(dateOfBirthElement != null) dateOfBirth = dateOfBirthElement.getAsString();
				    
				    val glookoCodeSElement = jsonObject.get("user_id");
				    if(glookoCodeSElement != null) glookoCodeS = glookoCodeSElement.getAsString();
				    
				    val emailElement = jsonObject.get("email_address");
				    if(emailElement != null) email = emailElement.getAsString();
				    
				    val mrnElement = jsonObject.get("mrn");
				    if(mrnElement != null) mrn = mrnElement.getAsString();
				    
				    val glookoCode = new GlookoCode(created, firstName,
							lastName, dateOfBirth, glookoCodeS, email, mrn);
					// TODO Writing into database (It should be GlookoManagerService later)
					interventionAdministrationManagerService.writeGlookoCode(glookoCode);
					response.setStatus(HttpServletResponse.SC_OK);
				}
				else if (jsonObject.get("dataType") != null) {
					String glookoCode = "", syncTimestamp = "", dataType = "";
					val notificationRecieved = new Date();
					
					val glookoCodeElement = jsonObject.get("glookoCode");
					if(glookoCodeElement != null) glookoCode = glookoCodeElement.getAsString();
					
					val syncTimestampElement = jsonObject.get("syncTimestamp");
					if(syncTimestampElement != null) syncTimestamp = syncTimestampElement.getAsString();
					
					val dataTypeElement = jsonObject.get("dataType");
					if(dataTypeElement != null) dataType = dataTypeElement.getAsString();
					
					if(dataType.equals("foods")) {
						val glookoFoodsAPINotification = new GlookoFoodsAPINotification(notificationRecieved, 
							glookoCode, syncTimestamp);
						// TODO Writing into database (It should be GlookoManagerService later)
						interventionAdministrationManagerService.writeGlookoFoodsApiNotification(glookoFoodsAPINotification);
					}
					else log.info("Ignoring the data notification as it is " + dataType);
					response.setStatus(HttpServletResponse.SC_OK);
				}
			} catch (Exception e) {
				log.error(e);
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			
		}
		
	}

	private String getBody(HttpServletRequest request) {
		StringBuilder bodyOfRequest = new StringBuilder();
		try {
			BufferedReader bufferedReader = request.getReader();
			
			String line;
			while((line = bufferedReader.readLine()) != null) {
				bodyOfRequest.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bodyOfRequest.toString();
	}

	private void logRequest(HttpServletRequest request) {
		logMethodHeader(request);
		logHeaders(request);
		logParameters(request);
	}

	private void logMethodHeader(HttpServletRequest request) {
		String methodheader = request.getMethod();
		log.debug(methodheader + " request");
	}

	private void logHeaders(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaderNames();
		if(headers == null) {
			log.debug("No headers.");
			return;
		}
		while(headers.hasMoreElements()) {
			log.debug("Header: " + headers.nextElement());
		}
	}
	
	private void logParameters(HttpServletRequest request) {
		Enumeration<String> parameters = request.getParameterNames();
		if(parameters == null) {
			log.debug("No headers.");
			return;
		}
		while(parameters.hasMoreElements()) {
			String parameter = parameters.nextElement();
			log.debug("Parameters: " + parameter + " " + request.getParameter(parameter));
		}
	}
}

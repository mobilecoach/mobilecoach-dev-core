package ch.ethz.mc.servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.ethz.mc.MC;
import ch.ethz.mc.conf.Constants;
import ch.ethz.mc.services.InterventionAdministrationManagerService;
import lombok.extern.log4j.Log4j2;

/**
 * Servlet implementation class SensorRecordingManage
 * 
 * @author Prabhakaran Santhanam
 */

@WebServlet(displayName = "SensorRecordingManager", urlPatterns = "/recording/active/*", asyncSupported = true, loadOnStartup = 1)
@Log4j2
public class SensorRecordingManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static InterventionAdministrationManagerService interventionAdministrationManagerService = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SensorRecordingManagerServlet() {
        super();
    }
    
    @Override
    public void init(final ServletConfig servletConfig)
			throws ServletException {
    	if (!MC.getInstance().isReady()) {
			log.error("Servlet {} can't be started. Context is not ready!",
					this.getClass());
			throw new ServletException("Context is not ready!");
		}
    	
    	if (Constants.isSensorRecordingManagerActive()) {
    		interventionAdministrationManagerService = MC.getInstance().getInterventionAdministrationManagerService();
    		log.info("Initializing (Sensor Manager) servlet...");
    		super.init(servletConfig);
    	} else log.info("Sensor Manager servlet not required.");
    	
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logRequest(request);
		response.getWriter().append("Sensor Manager url.");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * TODO improve this so that it is robust. 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logRequest(request); 
		String participantIdentifier;
		try {
			participantIdentifier = request.getParameter("participantIdentifier");
			
			if (participantIdentifier == null) {
				log.error("Participant identifier/user id is missing from the request");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
		} catch (Exception exception) {
			log.error("Could not process the parameters of the request: ", exception);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(interventionAdministrationManagerService.setLastContactPassiveSensingTimestampForParticipant(
				participantIdentifier)) response.setStatus(HttpServletResponse.SC_OK);
		else response.setStatus(HttpServletResponse.SC_ACCEPTED);
		
	}

	private void logRequest(HttpServletRequest request) {
		logMethodHeader(request);
		logHeaders(request);
		logParameters(request);
	}

	private void logMethodHeader(HttpServletRequest request) {
		String methodheader = request.getMethod();
		log.debug(methodheader + " request");
	}

	private void logHeaders(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaderNames();
		if(headers == null) {
			log.debug("No headers.");
			return;
		}
		while(headers.hasMoreElements()) {
			log.debug("Header: " + headers.nextElement());
		}
	}
	
	private void logParameters(HttpServletRequest request) {
		Enumeration<String> parameters = request.getParameterNames();
		if(parameters == null) {
			log.debug("No headers.");
			return;
		}
		while(parameters.hasMoreElements()) {
			String parameter = parameters.nextElement();
			log.debug("Parameters: " + parameter + " " + request.getParameter(parameter));
		}
	}
}

package ch.ethz.mc.servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.ethz.mc.MC;
import ch.ethz.mc.conf.Constants;
import ch.ethz.mc.conf.DeepstreamConstants;
import ch.ethz.mc.conf.ImplementationConstants;
import ch.ethz.mc.model.persistent.types.DialogOptionTypes;
import ch.ethz.mc.model.persistent.types.PushNotificationTypes;
import ch.ethz.mc.services.InterventionExecutionManagerService;
import lombok.extern.log4j.Log4j2;

/**
 * Servlet implementation class GlookoNotification
 * 
 * @author Prabhakaran Santhanam
 */

@WebServlet(displayName = "PushNotificationNotificationsListener", urlPatterns = "/push/notification/*", asyncSupported = true, loadOnStartup = 1)
@Log4j2
public class PushNotificationNotificationsListener extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static InterventionExecutionManagerService interventionExecutionManagerService = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PushNotificationNotificationsListener() {
        super();
    }
    
    @Override
    public void init(final ServletConfig servletConfig)
			throws ServletException {
    	if (!MC.getInstance().isReady()) {
			log.error("Servlet {} can't be started. Context is not ready!",
					this.getClass());
			throw new ServletException("Context is not ready!");
		}
    	
    	if (Constants.isPushNotificationsActive()) {
    		interventionExecutionManagerService = MC.getInstance().getInterventionExecutionManagerService();
    		log.info("Initializing (Push notification tokens listener) servlet...");
    		super.init(servletConfig);
    	} else log.info("Push notification tokens servlet not required.");
    	
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logRequest(request);
		response.getWriter().append("Push notification tokens' listener url.");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * TODO improve this so that it is robust. 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logRequest(request); 
		String secret = "", token, platform, deepstreamID;
		try {
			secret = request.getParameter("secret");
			token = request.getParameter("token");
			platform = request.getParameter("platform");
			deepstreamID = request.getParameter("deepstreamID");
			
			if (secret == null || token == null 
					|| deepstreamID == null || platform == null) {
				log.error("Atleast one of the parameters is not available");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
		} catch (Exception exception) {
			log.error("Could not process the parameters of the request: ", exception);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if (!secret.equals(Constants.getPushNotificationsTokensSecret())) {
			log.error("Unauthorized access.");
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		PushNotificationTypes platformType;
		
		if (platform
				.equals(DeepstreamConstants.PLATFORM_IOS)) {
			platformType = PushNotificationTypes.IOS;
		} else if (platform.equals(
				DeepstreamConstants.PLATFORM_ANDROID)) {
			platformType = PushNotificationTypes.ANDROID;
		} else {
			log.error("Platform not supported");
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		

		if (interventionExecutionManagerService
				.dialogOptionAddPushNotificationTokenBasedOnTypeAndData(
						DialogOptionTypes.EXTERNAL_ID,
						ImplementationConstants.DIALOG_OPTION_IDENTIFIER_FOR_DEEPSTREAM
								+ deepstreamID,
								platformType, token)) {
			log.debug("Updated push token if different");
			response.setStatus(HttpServletResponse.SC_OK);
		} else if (interventionExecutionManagerService
				.dialogOptionAddPushNotificationTokenBasedOnTypeAndData(
						DialogOptionTypes.EXTERNAL_ID,
						ImplementationConstants.DIALOG_OPTION_IDENTIFIER_FOR_DEEPSTREAM
								+ deepstreamID,
								platformType, token)) {
			log.debug("Updated push token if different");
			response.setStatus(HttpServletResponse.SC_ACCEPTED);
			
		} else {
			log.error("Problem while updating the token");
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		}
		
	}

	private void logRequest(HttpServletRequest request) {
		logMethodHeader(request);
		logHeaders(request);
		logParameters(request);
	}

	private void logMethodHeader(HttpServletRequest request) {
		String methodheader = request.getMethod();
		log.debug(methodheader + " request");
	}

	private void logHeaders(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaderNames();
		if(headers == null) {
			log.debug("No headers.");
			return;
		}
		while(headers.hasMoreElements()) {
			log.debug("Header: " + headers.nextElement());
		}
	}
	
	private void logParameters(HttpServletRequest request) {
		Enumeration<String> parameters = request.getParameterNames();
		if(parameters == null) {
			log.debug("No headers.");
			return;
		}
		while(parameters.hasMoreElements()) {
			String parameter = parameters.nextElement();
			log.debug("Parameters: " + parameter + " " + request.getParameter(parameter));
		}
	}
}

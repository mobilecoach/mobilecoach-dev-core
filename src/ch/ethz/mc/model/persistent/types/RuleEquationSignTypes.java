package ch.ethz.mc.model.persistent.types;

/* ##LICENSE## */
import java.util.ArrayList;
import java.util.List;

/**
 * Supported {@link RuleEquationSignTypes}
 *
 * @author Andreas Filler
 */
public enum RuleEquationSignTypes {
	CALCULATE_VALUE_BUT_RESULT_IS_ALWAYS_TRUE,
	CALCULATE_VALUE_BUT_RESULT_IS_ALWAYS_FALSE,
	CREATE_TEXT_BUT_RESULT_IS_ALWAYS_TRUE,
	CREATE_TEXT_BUT_RESULT_IS_ALWAYS_FALSE,
	CALCULATED_VALUE_IS_SMALLER_THAN,
	CALCULATED_VALUE_IS_SMALLER_OR_EQUAL_THAN,
	CALCULATED_VALUE_EQUALS,
	CALCULATED_VALUE_NOT_EQUALS,
	CALCULATED_VALUE_IS_BIGGER_OR_EQUAL_THAN,
	CALCULATED_VALUE_IS_BIGGER_THAN,
	CALCULATED_VALUE_MATCHES_REGULAR_EXPRESSION,
	CALCULATED_VALUE_NOT_MATCHES_REGULAR_EXPRESSION,
	CALCULATE_AMOUNT_OF_SELECT_MANY_VALUES,
	CALCULATE_SUM_OF_SELECT_MANY_VALUES_AND_TRUE_IF_SMALLER_THAN,
	CALCULATE_SUM_OF_SELECT_MANY_VALUES_AND_TRUE_IF_EQUALS,
	CALCULATE_SUM_OF_SELECT_MANY_VALUES_AND_TRUE_IF_BIGGER_THAN,
	TEXT_VALUE_EQUALS,
	TEXT_VALUE_NOT_EQUALS,
	TEXT_VALUE_MATCHES_REGULAR_EXPRESSION,
	TEXT_VALUE_NOT_MATCHES_REGULAR_EXPRESSION,
	TEXT_VALUE_MATCHES_REGULAR_EXPRESSION_CASE_SENSITIVE,
	TEXT_VALUE_NOT_MATCHES_REGULAR_EXPRESSION_CASE_SENSITIVE,
	TEXT_VALUE_FROM_SELECT_MANY_AT_POSITION,
	TEXT_VALUE_FROM_SELECT_MANY_AT_RANDOM_POSITION,
	TEXT_VALUE_FROM_JSON_BY_JSON_PATH,
	TEXT_VALUE_MATCHES_KEY,
	TEXT_VALUE_NOT_MATCHES_KEY,
	DATE_DIFFERENCE_VALUE_EQUALS,
	CALCULATE_DATE_DIFFERENCE_IN_DAYS_AND_TRUE_IF_ZERO,
	CALCULATE_DATE_DIFFERENCE_IN_MONTHS_AND_TRUE_IF_ZERO,
	CALCULATE_DATE_DIFFERENCE_IN_YEARS_AND_TRUE_IF_ZERO,
	CALCULATE_DATE_DIFFERENCE_IN_DAYS_AND_ALWAYS_TRUE,
	CALCULATE_DATE_DIFFERENCE_IN_MONTHS_AND_ALWAYS_TRUE,
	CALCULATE_DATE_DIFFERENCE_IN_YEARS_AND_ALWAYS_TRUE,
	STARTS_ITERATION_FROM_X_UP_TO_Y_AND_RESULT_IS_CURRENT,
	STARTS_REVERSE_ITERATION_FROM_X_DOWN_TO_Y_AND_RESULT_IS_CURRENT,
	GET_YEAR_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE,
	GET_MONTH_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE,
	GET_DAY_OF_MONTH_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE,
	GET_DAY_OF_WEEK_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE,
	CHECK_VALUE_IN_VARIABLE_ACROSS_INVTERVENTIONS_AND_TRUE_IF_DUPLICATE_FOUND,
	COUNT_PARTICIPANTS_HAVING_VARIABLE_WITH_VALUE_BUT_RESULT_IS_ALWAYS_TRUE,
	GET_VALUE_OF_A_VARIABLE_OF_A_PARTICIPANT_WITH_GIVEN_VARIABLE_AND_UNIQUE_VALUE,
	EXECUTE_JAVASCRIPT_IN_X_AND_STORE_VALUES_BUT_RESULT_IS_ALWAYS_TRUE,
	BGM_ASYNC_GET_GLUCOSE_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE,
	BGM_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE,
	BGM_DATA_SYNCED_IN_THIS_PERIOD,
	BGM_SWEETGOALS_GLUCOSE_MONITORING_BEHAVIOUR_DAILY_GOAL_ACHIEVED,
	BGM_SWEETGOALS_VALID_GLUCOSE_COUNTS_BUT_RESULT_ALWAYS_TRUE,
	BGM_IP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED,
	BGM_APP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED,
	CGM_ASYNC_GET_GLUCOSE_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE,
	CGM_DATA_SYNCED_IN_THIS_PERIOD,
	CGM_ASYNC_GET_WEARTIME_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE,
	CGM_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE,
	CGM_IP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED,
	CGM_APP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED,
	IP_ASYNC_GET_GLUCOSE_NON_MANUAL_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE,
	IP_ASYNC_GET_CARBS_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE,
	IP_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE,
	IP_DATA_SYNCED_IN_THIS_PERIOD,
	GLOOKO_APP_ASYNC_GET_CARBS_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE,
	GLOOKO_APP_DATA_SYNCED_IN_THIS_PERIOD,
	GLOOKO_APP_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE,
	SWEETGOALS_MEALTIME_BEHAVIOUR_VALID_PAIR_COUNTS_BUT_RESULT_ALWAYS_TRUE,
	GLUCOSE_DATA_CONTAINS_ABOVE_OR_EQUAL_TO_GIVEN_VALUE,
	GLUCOSE_DATA_CONTAINS_BELOW_GIVEN_VALUE,
	CTPAYER_ASYNC_PAY_PARTICIPANT_AND_GET_STATUS_BUT_RESULT_ALWAYS_TRUE;

	private static List<RuleEquationSignTypes>	calculatedEquationSigns	 = null;
	private static List<RuleEquationSignTypes>	iteratorEquationSigns	 = null;
	private static List<RuleEquationSignTypes>	asyncGlookoEquationSigns = null;
	private static List<RuleEquationSignTypes>	syncGlookoEquationSigns	 = null;

	@Override
	public String toString() {
		return name().toLowerCase().replace("_", " ");
	}

	public synchronized boolean isCalculatedEquationSignType() {
		// Create list at first check
		if (calculatedEquationSigns == null) {
			calculatedEquationSigns = new ArrayList<RuleEquationSignTypes>();
			calculatedEquationSigns
					.add(CALCULATE_VALUE_BUT_RESULT_IS_ALWAYS_TRUE);
			calculatedEquationSigns
					.add(CALCULATE_VALUE_BUT_RESULT_IS_ALWAYS_FALSE);
			calculatedEquationSigns.add(CALCULATED_VALUE_IS_SMALLER_THAN);
			calculatedEquationSigns
					.add(CALCULATED_VALUE_IS_SMALLER_OR_EQUAL_THAN);
			calculatedEquationSigns.add(CALCULATED_VALUE_EQUALS);
			calculatedEquationSigns.add(CALCULATED_VALUE_NOT_EQUALS);
			calculatedEquationSigns
					.add(CALCULATED_VALUE_IS_BIGGER_OR_EQUAL_THAN);
			calculatedEquationSigns.add(CALCULATED_VALUE_IS_BIGGER_THAN);
			calculatedEquationSigns
					.add(STARTS_ITERATION_FROM_X_UP_TO_Y_AND_RESULT_IS_CURRENT);
			calculatedEquationSigns.add(
					RuleEquationSignTypes.STARTS_REVERSE_ITERATION_FROM_X_DOWN_TO_Y_AND_RESULT_IS_CURRENT);
		}

		if (calculatedEquationSigns.contains(this)) {
			return true;
		} else {
			return false;
		}
	}

	public synchronized boolean isIteratorEquationSignType() {
		// Create list at first check
		if (iteratorEquationSigns == null) {
			iteratorEquationSigns = new ArrayList<RuleEquationSignTypes>();
			iteratorEquationSigns
					.add(STARTS_ITERATION_FROM_X_UP_TO_Y_AND_RESULT_IS_CURRENT);
			iteratorEquationSigns.add(
					RuleEquationSignTypes.STARTS_REVERSE_ITERATION_FROM_X_DOWN_TO_Y_AND_RESULT_IS_CURRENT);
		}

		if (iteratorEquationSigns.contains(this)) {
			return true;
		} else {
			return false;
		}
	}

	public synchronized boolean isRegularExpressionBasedEquationSignType() {
		if (this == TEXT_VALUE_MATCHES_REGULAR_EXPRESSION
				|| this == TEXT_VALUE_NOT_MATCHES_REGULAR_EXPRESSION 
				|| this == TEXT_VALUE_MATCHES_REGULAR_EXPRESSION_CASE_SENSITIVE
				|| this == TEXT_VALUE_NOT_MATCHES_REGULAR_EXPRESSION_CASE_SENSITIVE) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isCalculatedAndRegularExpressionBasedEquationSignType() {
		if (this == CALCULATED_VALUE_MATCHES_REGULAR_EXPRESSION
				|| this == CALCULATED_VALUE_NOT_MATCHES_REGULAR_EXPRESSION) {
			return true;
		} else {
			return false;
		}
	}

	public synchronized boolean isACrossInterventionVariableComparisionBasedEquationSignType() {
		if (this == CHECK_VALUE_IN_VARIABLE_ACROSS_INVTERVENTIONS_AND_TRUE_IF_DUPLICATE_FOUND) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isCountParticipantsHavingVariableWithValueEquationSignType() {
		if (this == COUNT_PARTICIPANTS_HAVING_VARIABLE_WITH_VALUE_BUT_RESULT_IS_ALWAYS_TRUE) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isInterParticipantVariableRule() {
		if (this == GET_VALUE_OF_A_VARIABLE_OF_A_PARTICIPANT_WITH_GIVEN_VARIABLE_AND_UNIQUE_VALUE) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isTimeStampToCalendarRule() {
		if (this == GET_YEAR_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE ||
				this == GET_MONTH_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE ||
				this == GET_DAY_OF_MONTH_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE ||
				this == GET_DAY_OF_WEEK_FROM_UNIX_EPOCH_TIMESTAMP_AND_RESULT_IS_ALWAYS_TRUE) {
			return true;
		} else {
			return false;
		}
	} 

	public synchronized boolean isJavaScriptBasedRule() {
		if (this == RuleEquationSignTypes.EXECUTE_JAVASCRIPT_IN_X_AND_STORE_VALUES_BUT_RESULT_IS_ALWAYS_TRUE) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isAsyncGlookoBasedRule() {
		if (asyncGlookoEquationSigns == null) {
			asyncGlookoEquationSigns = new ArrayList<RuleEquationSignTypes>();
			// BGM
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_ASYNC_GET_GLUCOSE_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE);
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE);
			// CGM
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.CGM_ASYNC_GET_GLUCOSE_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE);
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.CGM_ASYNC_GET_WEARTIME_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE);
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.CGM_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE);
			// IP
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.IP_ASYNC_GET_GLUCOSE_NON_MANUAL_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE);
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.IP_ASYNC_GET_CARBS_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE);
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.IP_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE);
			// Glooko app
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.GLOOKO_APP_ASYNC_GET_CARBS_AND_TIMESTAMP_IN_DATE_RANGE_BUT_RESULT_ALWAYS_TRUE);
			asyncGlookoEquationSigns.add(RuleEquationSignTypes.GLOOKO_APP_ASYNC_GET_DATA_IN_DATE_RANGE_AND_STORE_AS_FILE_BUT_RESULT_ALWAYS_TRUE);
		}
		
		if (asyncGlookoEquationSigns.contains(this)) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isSyncGlookoBasedRule() {
		if (syncGlookoEquationSigns == null) {
			syncGlookoEquationSigns = new ArrayList<RuleEquationSignTypes>();
			// BGM
			syncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_DATA_SYNCED_IN_THIS_PERIOD);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_SWEETGOALS_GLUCOSE_MONITORING_BEHAVIOUR_DAILY_GOAL_ACHIEVED);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_IP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_APP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.BGM_SWEETGOALS_VALID_GLUCOSE_COUNTS_BUT_RESULT_ALWAYS_TRUE);
			// CGM
			syncGlookoEquationSigns.add(RuleEquationSignTypes.CGM_DATA_SYNCED_IN_THIS_PERIOD);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.CGM_IP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.CGM_APP_SWEETGOALS_MEALTIME_BEHAVIOUR_DAILY_GOAL_ACHIEVED);
			// IP
			syncGlookoEquationSigns.add(RuleEquationSignTypes.IP_DATA_SYNCED_IN_THIS_PERIOD);
			// Glooko app
			syncGlookoEquationSigns.add(RuleEquationSignTypes.GLOOKO_APP_DATA_SYNCED_IN_THIS_PERIOD);
			// General
			syncGlookoEquationSigns.add(RuleEquationSignTypes.SWEETGOALS_MEALTIME_BEHAVIOUR_VALID_PAIR_COUNTS_BUT_RESULT_ALWAYS_TRUE);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.GLUCOSE_DATA_CONTAINS_ABOVE_OR_EQUAL_TO_GIVEN_VALUE);
			syncGlookoEquationSigns.add(RuleEquationSignTypes.GLUCOSE_DATA_CONTAINS_BELOW_GIVEN_VALUE);
		}
		
		if (syncGlookoEquationSigns.contains(this)) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized boolean isAsyncCTPayerBasedRule() {
	    if (this == RuleEquationSignTypes.CTPAYER_ASYNC_PAY_PARTICIPANT_AND_GET_STATUS_BUT_RESULT_ALWAYS_TRUE) 
	      return true;
	    return false;
	}
}

package ch.ethz.mc.model.persistent;

import java.util.Date;

import ch.ethz.mc.model.ModelObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * {@link ModelObject} to represent a {@link GlookoCode}
 * 
 * Glooko notifications received from Glooko framework when a user adds carbs in the app
 * 
 * @author Prabhakaran Santhanam
 */
@NoArgsConstructor
@AllArgsConstructor
public class GlookoFoodsAPINotification extends ModelObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3172832307203388233L;

	@Getter
	@Setter
	private Date notificationRecieved;
	
	@Getter
	@Setter
	private String glookoCode;
	
	@Getter
	@Setter
	private String syncTimestamp;
	
}

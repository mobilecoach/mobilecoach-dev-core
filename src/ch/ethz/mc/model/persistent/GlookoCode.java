package ch.ethz.mc.model.persistent;

import java.util.Date;

import ch.ethz.mc.conf.AdminMessageStrings;
import ch.ethz.mc.conf.Messages;
import ch.ethz.mc.model.ModelObject;
import ch.ethz.mc.model.ui.UIGlookoCode;
import ch.ethz.mc.model.ui.UIModelObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.val;

/**
 * {@link ModelObject} to represent a {@link GlookoCode}
 * 
 * Glooko code received from Glooko framework during the pro-connect phase of Glooko
 * 
 * @author Prabhakaran Santhanam
 */
@NoArgsConstructor
@AllArgsConstructor
public class GlookoCode extends ModelObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2273569466215469503L;
	
	@Getter
	@Setter
	private Date created;
	
	@Getter
	@Setter
	private String firstName;
	
	@Getter
	@Setter
	private String lastName;
	
	@Getter
	@Setter
	private String dateOfBirth;
	
	@Getter
	@Setter
	private String glookoCodeS;
	
	@Getter
	@Setter
	private String email;
	
	@Getter
	@Setter
	private String mrn;
	
	@Override
	public UIModelObject toUIModelObject() {
		final val uiGlookoCode = new UIGlookoCode(
				created, 
				firstName.equals("")? 
						Messages.getAdminString(AdminMessageStrings.UI_MODEL__NOT_SET):firstName, 
				lastName.equals("")? 
						Messages.getAdminString(AdminMessageStrings.UI_MODEL__NOT_SET):lastName, 
				dateOfBirth.equals("")? 
						Messages.getAdminString(AdminMessageStrings.UI_MODEL__NOT_SET):dateOfBirth,
				glookoCodeS.equals("")? 
						Messages.getAdminString(AdminMessageStrings.UI_MODEL__NOT_SET):glookoCodeS, 
				email.equals("")? 
						Messages.getAdminString(AdminMessageStrings.UI_MODEL__NOT_SET):email, 
				mrn.equals("")? 
						Messages.getAdminString(AdminMessageStrings.UI_MODEL__NOT_SET):mrn);
		uiGlookoCode.setRelatedModelObject(this);
		return uiGlookoCode;
	}

}

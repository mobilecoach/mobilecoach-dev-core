package ch.ethz.mc.model.ui;

/* ##LICENSE## */
import java.util.Date;

import com.vaadin.data.fieldgroup.PropertyId;

import ch.ethz.mc.conf.AdminMessageStrings;
import ch.ethz.mc.model.persistent.GlookoCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class UIGlookoCode extends UIModelObject {
	// NOTE: The String values have to fit the name of the variables
	public static final String	CREATED					= "created";
	public static final String	FIRST_NAME				= "firstName";
	public static final String	LAST_NAME				= "lastName";
	public static final String	DATE_OF_BIRTH			= "dateOfBirth";
	public static final String	GLOOKO_CODE				= "glookoCodeS";
	public static final String	EMAIL					= "email";
	public static final String	MRN						= "mrn";
	
	@PropertyId(CREATED)
	private Date				created;

	@PropertyId(FIRST_NAME)
	private String				firstName;
	
	@PropertyId(LAST_NAME)
	private String				lastName;
	
	@PropertyId(DATE_OF_BIRTH)
	private String				dateOfBirth;
	
	@PropertyId(GLOOKO_CODE)
	private String				glookoCodeS;
	
	@PropertyId(EMAIL)
	private String				email;
	
	@PropertyId(MRN)
	private String				mrn;
	
	public static Object[] getVisibleColumns() {
		return new Object[] { CREATED, FIRST_NAME, LAST_NAME, 
				DATE_OF_BIRTH, GLOOKO_CODE, EMAIL, MRN};
	}
	
	public static String[] getColumnHeaders() {
		return new String[] { 
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__CREATED),
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__FIRST_NAME),
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__LAST_NAME),
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__DATE_OF_BIRTH),
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__CODE),
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__EMAIL),
				localize(AdminMessageStrings.UI_GLOOKO_CODE_COLUMNS__MRN)};
	}
	
	public static String getSortColumn() {
		return CREATED;
	}
	
	@Override
	public String toString() {
		return getRelatedModelObject(GlookoCode.class).getGlookoCodeS();
	}
}

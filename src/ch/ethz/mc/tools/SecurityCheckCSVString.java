package ch.ethz.mc.tools;

public class SecurityCheckCSVString {
	
	// Change special symbols =, +, -, @, | to codes
	public static String encodeSpecialSymbolsAtStartToCode(String inputEntry) {
		String encodedEntry;
		if (inputEntry.length() > 0) {
			if (inputEntry.charAt(0) == '=')
				encodedEntry = "___equal___" + inputEntry.substring(1);
			else if (inputEntry.charAt(0) == '|') 
				encodedEntry = "___pipe___" + inputEntry.substring(1);
			else if (inputEntry.charAt(0) == '+') 
				encodedEntry = "___plus___" + inputEntry.substring(1);
			else if (inputEntry.charAt(0) == '-') 
				encodedEntry = "___minus___" + inputEntry.substring(1);
			else if (inputEntry.charAt(0) == '@') 
				encodedEntry = "___at___" + inputEntry.substring(1);
			else 
				encodedEntry = inputEntry;
			return encodedEntry;
		}
		else return inputEntry;
	}
	
	// Change codes backk to special symbols =, +, -, @, | 
		public static String decodeCodeAtStartToSpecialSymbols(String inputEntry) {
			String decodedEntry;
			if (inputEntry.length() > 0) {
				if (inputEntry.startsWith("___equal___"))
					decodedEntry = inputEntry.replaceFirst("___equal___", "=");
				else if (inputEntry.startsWith("___pipe___")) 
					decodedEntry = inputEntry.replaceFirst("___pipe___", "|");
				else if (inputEntry.startsWith("___plus___"))
					decodedEntry = inputEntry.replaceFirst("___plus___", "+");
				else if (inputEntry.startsWith("___minus___"))
					decodedEntry = inputEntry.replaceFirst("___minus___", "-");
				else if (inputEntry.startsWith("___at___"))
					decodedEntry = inputEntry.replaceFirst("___at___", "@");
				else 
					decodedEntry = inputEntry;
				return decodedEntry;
			}
			else return inputEntry;
		}

}

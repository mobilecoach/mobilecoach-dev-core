package ch.ethz.mc.tools;

import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;

import de.taimos.totp.TOTP;

// Referred from https://github.com/IhorSokolyk/google-2fa

public class TOTPHelper {
	
	public static String generateTOTPCode(final String secretKey) {
		Base32 base32 = new Base32();
	    byte[] bytes = base32.decode(secretKey);
	    return TOTP.getOTP(Hex.encodeHexString(bytes));
	}

	public static String generateNewSecretKey() {
		SecureRandom random = new SecureRandom();
	    byte[] bytes = new byte[20];
	    random.nextBytes(bytes);
	    Base32 base32 = new Base32();
	    return base32.encodeToString(bytes);
	}

}

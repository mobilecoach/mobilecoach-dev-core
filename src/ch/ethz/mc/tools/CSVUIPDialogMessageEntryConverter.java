package ch.ethz.mc.tools;

/* ##LICENSE## */
import lombok.val;
import ch.ethz.mc.model.ui.results.UIDialogMessageWithParticipantForResults;

import com.googlecode.jcsv.writer.CSVEntryConverter;

/**
 * Converter to convert {@link UIDialogMessageWithParticipantForResults} to CSV
 * 
 * @author Andreas Filler
 */
public class CSVUIPDialogMessageEntryConverter
		implements CSVEntryConverter<UIDialogMessageWithParticipantForResults> {

	@Override
	public String[] convertEntry(
			final UIDialogMessageWithParticipantForResults uiDialogMessage) {
		return new String[] { SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
					uiDialogMessage.getParticipantId()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getParticipantName()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getLanguage()), 
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getGroup()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getOrder()), 
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getStatus()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getSenderType()), 
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getType()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						clean(uiDialogMessage.getMessage())),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getShouldBeSentTimestamp()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getSentTimestamp()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						clean(uiDialogMessage.getAnswer())),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						clean(uiDialogMessage.getRawAnswer())),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getAnswerReceivedTimestamp()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getManuallySent()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getContainsMediaContent()),
				SecurityCheckCSVString.encodeSpecialSymbolsAtStartToCode(
						uiDialogMessage.getMediaContentViewed()) };
	}

	private String clean(final String value) {
		return value.replace("\n", "").replace("\r", "");
	}

	public static UIDialogMessageWithParticipantForResults getHeaders() {
		val columnHeaders = UIDialogMessageWithParticipantForResults
				.getColumnHeaders();
		return new UIDialogMessageWithParticipantForResults(columnHeaders[0],
				columnHeaders[1], columnHeaders[2], columnHeaders[3],
				columnHeaders[4], columnHeaders[5], columnHeaders[6],
				columnHeaders[7], columnHeaders[8], columnHeaders[9],
				columnHeaders[10], columnHeaders[11], columnHeaders[12],
				columnHeaders[13], columnHeaders[14], columnHeaders[15],
				columnHeaders[16], columnHeaders[17], columnHeaders[18]);
	}
}
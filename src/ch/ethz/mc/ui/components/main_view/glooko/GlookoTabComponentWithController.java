package ch.ethz.mc.ui.components.main_view.glooko;


/* ##LICENSE## */
import org.bson.types.ObjectId;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import ch.ethz.mc.conf.AdminMessageStrings;
import ch.ethz.mc.model.persistent.GlookoCode;
import ch.ethz.mc.model.ui.UIGlookoCode;
import ch.ethz.mc.ui.components.basics.ShortStringEditComponent;
import lombok.val;
import lombok.extern.log4j.Log4j2;

/**
 * Extends the Glooko tab component with a controller
 * 
 * @author Prabhakaran Santhanam
 */
@SuppressWarnings("serial")
@Log4j2
public class GlookoTabComponentWithController
		extends GlookoTabComponent {

	private UIGlookoCode									selectedUIGlookoCode			= null;

	private final BeanContainer<ObjectId, UIGlookoCode>	beanContainer;

	public GlookoTabComponentWithController() {
		super();

		// table options
		val glookoEditComponent = getGlookoEditComponent();
		val glookoCodeTable = glookoEditComponent.getGlookoCodeTable();

		// table content
		beanContainer = createBeanContainerForModelObjects(UIGlookoCode.class,
				getInterventionAdministrationManagerService()
						.getAllGlookoCodes());

		glookoCodeTable.setContainerDataSource(beanContainer);
		glookoCodeTable.setSortContainerPropertyId(UIGlookoCode.getSortColumn());
		glookoCodeTable.setVisibleColumns(UIGlookoCode.getVisibleColumns());
		glookoCodeTable.setColumnHeaders(UIGlookoCode.getColumnHeaders());

		// handle selection change
		glookoCodeTable.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(final ValueChangeEvent event) {
				val objectId = glookoCodeTable.getValue();
				if (objectId == null) {
					glookoEditComponent.setNothingSelected();
					selectedUIGlookoCode = null;
				} else {
					selectedUIGlookoCode = getUIModelObjectFromTableByObjectId(
							glookoCodeTable, UIGlookoCode.class, objectId);
					glookoEditComponent.setSomethingSelected();
				}
			}
		});

		// handle buttons
		val buttonClickListener = new ButtonClickListener();
		glookoEditComponent.getRefreshButton()
				.addClickListener(buttonClickListener);
		glookoEditComponent.getCopyCodeButton()
				.addClickListener(buttonClickListener);
		glookoEditComponent.getDeleteButton()
				.addClickListener(buttonClickListener);
	}

	private class ButtonClickListener implements Button.ClickListener {

		@Override
		public void buttonClick(final ClickEvent event) {
			val glookoEditComponent = getGlookoEditComponent();

			if (event.getButton() == glookoEditComponent
					.getCopyCodeButton()) {
				copyCode();
			} else if (event.getButton() == glookoEditComponent
					.getDeleteButton()) {
				deleteCode();
			} else if (event.getButton() == glookoEditComponent
					.getRefreshButton()) {
				refreshTable();
			}
		}
	}

	public void copyCode() {
		log.debug("Copying glooko code window");
		
		if(selectedUIGlookoCode != null) {
			val codeToCopy = selectedUIGlookoCode.getGlookoCodeS();
			log.debug("Code copied is " + codeToCopy);
			
			//TODO MAke the model string not editable
			showModalStringValueEditWindow(
					AdminMessageStrings.ABSTRACT_STRING_EDITOR_WINDOW__COPY_GLOOKO_CODE,
					codeToCopy, null, new ShortStringEditComponent(), 
					new ExtendableButtonClickListener() {
						@Override
						public void buttonClick(final ClickEvent event) {
							try {
								val newValue = getStringValue();
								if(!newValue.equals(codeToCopy)) {
									
								}

							} catch (final Exception e) {
								handleException(e);
								return;
							}
							closeWindow();
						}
					}, null);
		}
		else {
			getAdminUI().showInformationNotification(
					AdminMessageStrings.NOTIFICATION__GLOOKO_CODE_NOT_COPIED);
		}
	}

	public void deleteCode() {
		log.debug("Delete glooko code");
		showConfirmationWindow(new ExtendableButtonClickListener() {

			@Override
			public void buttonClick(final ClickEvent event) {
				try {
					val selectedGlookoCode = selectedUIGlookoCode
							.getRelatedModelObject(GlookoCode.class);
					log.debug("Got the related model object");
					// Delete glooko code
					// TODO via Glooko manager service
					getInterventionAdministrationManagerService()
							.glookoCodeDelete(selectedGlookoCode);
					log.debug("Deleted glooko code in database");
				} catch (final Exception e) {
					closeWindow();
					handleException(e);
					return;
				}

				// Adapt UI
				getGlookoEditComponent().getGlookoCodeTable()
						.removeItem(selectedUIGlookoCode
								.getRelatedModelObject(GlookoCode.class)
								.getId());
				log.debug("Deleted glooko code in UI");
				getAdminUI().showInformationNotification(
						AdminMessageStrings.NOTIFICATION__GLOOKO_CODE_DELETED);

				closeWindow();
			}
		}, null);
	}
	
	public void refreshTable () {
		log.debug("Refresh the table");
		refreshBeanContainer(beanContainer, UIGlookoCode.class,
				GlookoCode.class,
				getInterventionAdministrationManagerService()
						.getAllGlookoCodes());
	}
}

package ch.ethz.mc.ui.components.main_view.glooko;

/* ##LICENSE## */
import com.vaadin.annotations.AutoGenerated;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import ch.ethz.mc.conf.AdminMessageStrings;
import ch.ethz.mc.ui.components.AbstractCustomComponent;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Provides the Glooko edit component
 * 
 * @author Prabhakaran Santhanam
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
public class GlookoEditComponent extends AbstractCustomComponent {

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	@AutoGenerated
	private VerticalLayout		mainLayout;
	@AutoGenerated
	private HorizontalLayout	buttonLayout;
	@AutoGenerated
	private Button				refreshButton;
	@AutoGenerated
	private Button				copyCodeButton;
	@AutoGenerated
	private Button				deleteButton;
	@AutoGenerated
	private Table				glookoCodeTable;

	/**
	 * The constructor should first build the main layout, set the composition
	 * root and then do any custom initialization.
	 * 
	 * The constructor will not be automatically regenerated by the visual
	 * editor.
	 */
	protected GlookoEditComponent() {
		buildMainLayout();
		setCompositionRoot(mainLayout);

		// manually added
		localize(refreshButton,
				AdminMessageStrings.GENERAL__REFRESH);
		localize(copyCodeButton,
				AdminMessageStrings.GLOOKO_TAB__COPY_CODE);
		localize(deleteButton, AdminMessageStrings.GENERAL__DELETE);

		// set button start state (except for refresh)
		setNothingSelected();
		refreshButton.setEnabled(true);
	}

	protected void setNothingSelected() {
		copyCodeButton.setEnabled(false);
		deleteButton.setEnabled(false);
	}

	protected void setSomethingSelected() {
		refreshButton.setEnabled(true);
		copyCodeButton.setEnabled(true);
		deleteButton.setEnabled(true);
	}

	@AutoGenerated
	private VerticalLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new VerticalLayout();
		mainLayout.setImmediate(false);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("-1px");
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);

		// top-level component properties
		setWidth("100.0%");
		setHeight("-1px");

		// accountsTable
		glookoCodeTable = new Table();
		glookoCodeTable.setImmediate(false);
		glookoCodeTable.setWidth("100.0%");
		glookoCodeTable.setHeight("250px");
		mainLayout.addComponent(glookoCodeTable);

		// buttonLayout
		buttonLayout = buildButtonLayout();
		mainLayout.addComponent(buttonLayout);

		return mainLayout;
	}

	@AutoGenerated
	private HorizontalLayout buildButtonLayout() {
		// common part: create layout
		buttonLayout = new HorizontalLayout();
		buttonLayout.setImmediate(false);
		buttonLayout.setWidth("-1px");
		buttonLayout.setHeight("-1px");
		buttonLayout.setMargin(false);
		buttonLayout.setSpacing(true);
		
		// refresh button
		refreshButton = new Button();
		refreshButton.setCaption("!!! Refresh");
		refreshButton.setIcon(new ThemeResource("img/loading-icon-small.png"));
		refreshButton.setImmediate(true);
		refreshButton.setWidth("100px");
		refreshButton.setHeight("-1px");
		buttonLayout.addComponent(refreshButton);

		// Copy code button
		copyCodeButton = new Button();
		copyCodeButton.setCaption("!!! Copy code");
		copyCodeButton.setIcon(new ThemeResource("img/copy-icon-small.png"));
		copyCodeButton.setImmediate(true);
		copyCodeButton.setWidth("100px");
		copyCodeButton.setHeight("-1px");
		buttonLayout.addComponent(copyCodeButton);

		// deleteButton
		deleteButton = new Button();
		deleteButton.setCaption("!!! Delete");
		deleteButton.setIcon(new ThemeResource("img/delete-icon-small.png"));
		deleteButton.setImmediate(true);
		deleteButton.setWidth("100px");
		deleteButton.setHeight("-1px");
		buttonLayout.addComponent(deleteButton);

		return buttonLayout;
	}

}

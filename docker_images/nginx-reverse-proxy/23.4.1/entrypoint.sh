#!/bin/bash

chown -R nginx:nginx /sockets
chmod -R 755 /sockets

exec nginx -g 'daemon off;'